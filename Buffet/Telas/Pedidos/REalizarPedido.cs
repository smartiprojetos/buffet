﻿using Buffet.Db.Agenda;
using Buffet.Db.Clientes;
using Buffet.Db.Funcionarios;
using Buffet.Db.Pedidos;
using Buffet.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class REalizarPedido : Form
    {
        int pk;
        int fk;
        public REalizarPedido()
        {
            InitializeComponent();
          
            btnalterar.Visible = false;
            btnalterar.Enabled = false;
            lblalterar.Visible = false;
        }
       
        //void CarregarCombos()
        //{
        //    Business_agenda business = new Business_agenda();
        //    List<DTO_agenda> lista = business.Listar();

        //    data.ValueMember = nameof(DTO_agenda.ID_agenda);
        //    data.DisplayMember = nameof(DTO_agenda.dt_datareserva);
        //    data.DataSource = lista;
        //}

        private void label4_Click(object sender, EventArgs e)
        {
            Telas.TesteLogin oi8 = new Telas.TesteLogin();
            oi8.Show();
        }

        private void label4_Click_1(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
         
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void panel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
           
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        //private void agenda()
        //{
        ////    DTO_agenda dTO_Agenda = new DTO_agenda();
        ////    Business_agenda agenda = new Business_agenda();

        ////    label4.Text = dTO_Agenda.dt_datareserva;
        ////    fk = dTO_Agenda.ID_agenda;

        //}
        decimal result = 0;
            private void Calculos()
        {
            decimal valor = Convert.ToDecimal(txtvalor.Text);
            decimal desconto = Convert.ToDecimal(txtdesconto.Text);
            CalculoPedido calculoPedido = new CalculoPedido();
           result = calculoPedido.Calculo(valor, desconto);

            lblValorTotal.Text = result.ToString();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {

                Calculos();

            }
            catch (Exception)
            {
                MessageBox.Show("Os Campos Valor e Desconto só aceitam números!",
                                "Informação",
                                MessageBoxButtons.AbortRetryIgnore,
                                MessageBoxIcon.Information);

            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            

            //try
            //{

            
                DTO_Pedidos dto = new DTO_Pedidos();
                DTO_Clientes dtocliente = new DTO_Clientes();
                DTO_mes dtomes = new DTO_mes();


                dto.desconto = Convert.ToDecimal(txtdesconto.Text);
                dto.descricao = txtdescricao.Text;
                dto.local = txtlocal.Text;
                dto.Valor = Convert.ToDecimal(txtvalor.Text);
                dto.Valor_Total = result;
                dto.forma_Pagamento = cboFormaPagar.Text;
                dto.hr_horario = txthora.Text;
                dto.obs_observacao = txtobs.Text;
                dto.dt_dataatual = DateTime.Now;
                dto.dt_datareserva = dtpfesta.Value;
                dto.fk_cliente = pk;
                dto.id_mes = dtomes.id_mes;

            ////agenda();

            ////precisa fazer com que cadastre um id certo da agenda
            //dto.fk_agenda = agenda.ID_agenda;



            //int pk = UserSession.UsuarioLogado.ID_Funcionario;
            //dto.fk_Funcionario = Convert.ToInt32(txtidusuario.Text);


            Business_Pedidos business = new Business_Pedidos();
                business.Salvar(dto);

                //teste




                MessageBox.Show("Reserva efetuada com sucesso!", 
                                "Magic Buffet", 
                                MessageBoxButtons.OK, 
                                MessageBoxIcon.Information);

                DialogResult re = MessageBox.Show("Deseja enviar uma confirmção por E-mail",
                            "Magic Buffet",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question);
                Email(re);

                Hide();

            //}
            //catch (Exception)
            //{
            //    MessageBox.Show("Certifique-se de que prencheu todos os campos corretamente!",
            //                    "Informação", 
            //                    MessageBoxButtons.AbortRetryIgnore,
            //                    MessageBoxIcon.Information);

            //}

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Reserva atualizada com sucesso!",
                            "Magic Buffet", 
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
            Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Hide();
        }

        public void LoadScreenAlterar(DTO_Pedidos dto)
        {
           
        }

       

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Telas.Agenda.calendario agenda = new Telas.Agenda.calendario();
            agenda.Show();

        }


        private void txtCPFcliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

      
        private void Email(DialogResult re)
        {
            

            if (re == DialogResult.Yes)
            {
                Business_Cliente buscliente = new Business_Cliente();
                string para = txtemail.Text;
                string copiaoculta = "smarti7empresa@gmail.com";
                string copia = "smarti7empresa@gmail.com"; 
                string assunto = "Confirmação de Agendamento";
                bool comhtml = false;
                string mensagem =
                    "Olá, informamos que "
                    + txtnomecliente.Text
                    + "com o CPF de numero"
                    + txtCPFcliente.Text
                    + " está realizando o pedido de uma festa. Com a descrição de : "
                    + txtdescricao.Text
                    + "Tendo como local :"
                    + txtlocal.Text
                    + " Com o valor total de"
                    + lblValorTotal.Text
                    + "Sendo paga : "
                    + cboFormaPagar.SelectedItem.ToString()
                    +"Caso não seja voce farvor entrar em contato.";
                   

                Email email = new Email();
                email.EnviarGmail(para, copia, copiaoculta, assunto, mensagem, comhtml);

                MessageBox.Show("Confirmação por email enviada com sucesso",
                                "Magic Buffet",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            
        }

        // terminar a parte de puxar os valores para o txt nome!!!
        //mudar o evento
        private void txtCPFcliente_TextChanged(object sender, EventArgs e)
        {
            DTO_Clientes dtocliente = new DTO_Clientes();
            dtocliente.CPF = txtCPFcliente.Text;

            Business_Cliente buscliente = new Business_Cliente();
            DTO_Clientes nha = buscliente.Consultar(dtocliente.CPF);

            txtnomecliente.Text = nha.Nome;
        }

        private void txtCPFcliente_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            
        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            Business_Cliente cliente = new Business_Cliente();
            DTO_Clientes dto = cliente.Consultar(txtCPFcliente.Text);

            txtnomecliente.Text = dto.Nome;
            txtemail.Text = dto.Email;
            pk = dto.ID;
        }

        private void REalizarPedido_Load(object sender, EventArgs e)
        {

        }

        private void txtCPFcliente_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            DTO_Clientes dtocliente = new DTO_Clientes();
            dtocliente.CPF = txtCPFcliente.Text;
            Business_Cliente buscliente = new Business_Cliente();
            string cpf = txtCPFcliente.Text;

            buscliente.Consultar(cpf);

            txtnomecliente.Text = dtocliente.Nome;
        }

        //public void LoadScreen(DateTime datahora, string hora)
        //{
        //    mktdatahora.Value= datahora;
        //    txthoras.Text = hora;
        //}
        //private void LoadScreen2()
        //{



        //    mktdatahora.Value = Horario.Valor;
        //    txthoras.Text = Horario.horas;
        //}

        private void label16_Click(object sender, EventArgs e)
        {
            

        }
    }
}
