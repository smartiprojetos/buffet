﻿namespace Buffet.Telas
{
    partial class CadastroFuncionarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CadastroFuncionarios));
            this.cbocargo = new System.Windows.Forms.ComboBox();
            this.txtbairro = new System.Windows.Forms.TextBox();
            this.txtcidade = new System.Windows.Forms.TextBox();
            this.txtcomplemento = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtendendereco = new System.Windows.Forms.TextBox();
            this.dtpdatasaida = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtnumero = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cbouf = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtcelular = new System.Windows.Forms.MaskedTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txttelefone = new System.Windows.Forms.MaskedTextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label28 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtobservacaofuncionarios = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtobservacoesendereco = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btncep = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.cbosituacao = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtusuario = new System.Windows.Forms.TextBox();
            this.txtsenha = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtcpf = new System.Windows.Forms.MaskedTextBox();
            this.txtcep = new System.Windows.Forms.MaskedTextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel11 = new System.Windows.Forms.Panel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label34 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.rbtnaotercerizados = new System.Windows.Forms.RadioButton();
            this.rbtsimtercerizados = new System.Windows.Forms.RadioButton();
            this.label33 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.rbtnaoestoque = new System.Windows.Forms.RadioButton();
            this.rbtsimestoque = new System.Windows.Forms.RadioButton();
            this.Fornecedor = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.rbtnaofornecedor = new System.Windows.Forms.RadioButton();
            this.rbtsimfornecedor = new System.Windows.Forms.RadioButton();
            this.label32 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.rbtnaofuncionario = new System.Windows.Forms.RadioButton();
            this.rbtsimfuncionario = new System.Windows.Forms.RadioButton();
            this.label31 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.rbtnaopedido = new System.Windows.Forms.RadioButton();
            this.rbtsimpedido = new System.Windows.Forms.RadioButton();
            this.label29 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.rdbnaocliente = new System.Windows.Forms.RadioButton();
            this.rdbsimcliente = new System.Windows.Forms.RadioButton();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.label30 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lbltitulo = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel11.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // cbocargo
            // 
            this.cbocargo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbocargo.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbocargo.FormattingEnabled = true;
            this.cbocargo.Items.AddRange(new object[] {
            "Cozinheiro",
            "Faxineiro",
            "Organizador de Eventos"});
            this.cbocargo.Location = new System.Drawing.Point(423, 28);
            this.cbocargo.Name = "cbocargo";
            this.cbocargo.Size = new System.Drawing.Size(212, 21);
            this.cbocargo.TabIndex = 7;
            this.cbocargo.SelectedIndexChanged += new System.EventHandler(this.cbocargo_SelectedIndexChanged);
            // 
            // txtbairro
            // 
            this.txtbairro.Location = new System.Drawing.Point(129, 292);
            this.txtbairro.Name = "txtbairro";
            this.txtbairro.Size = new System.Drawing.Size(323, 23);
            this.txtbairro.TabIndex = 19;
            // 
            // txtcidade
            // 
            this.txtcidade.Location = new System.Drawing.Point(288, 207);
            this.txtcidade.Name = "txtcidade";
            this.txtcidade.Size = new System.Drawing.Size(164, 23);
            this.txtcidade.TabIndex = 16;
            // 
            // txtcomplemento
            // 
            this.txtcomplemento.Location = new System.Drawing.Point(11, 292);
            this.txtcomplemento.Name = "txtcomplemento";
            this.txtcomplemento.Size = new System.Drawing.Size(109, 23);
            this.txtcomplemento.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(423, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 17);
            this.label10.TabIndex = 49;
            this.label10.Text = "Cargo";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 272);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 17);
            this.label8.TabIndex = 47;
            this.label8.Text = "Complemento";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(354, 230);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 17);
            this.label4.TabIndex = 46;
            this.label4.Text = "N°";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(315, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 17);
            this.label6.TabIndex = 43;
            this.label6.Text = "CPF";
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(7, 30);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(299, 23);
            this.txtnome.TabIndex = 2;
            this.txtnome.TextChanged += new System.EventHandler(this.txtnome_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 37;
            this.label1.Text = "Funcionário";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(423, 123);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 17);
            this.label7.TabIndex = 57;
            this.label7.Text = "Data Saída";
            // 
            // txtendendereco
            // 
            this.txtendendereco.Location = new System.Drawing.Point(11, 249);
            this.txtendendereco.Name = "txtendendereco";
            this.txtendendereco.Size = new System.Drawing.Size(340, 23);
            this.txtendendereco.TabIndex = 17;
            // 
            // dtpdatasaida
            // 
            this.dtpdatasaida.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpdatasaida.Location = new System.Drawing.Point(423, 140);
            this.dtpdatasaida.Name = "dtpdatasaida";
            this.dtpdatasaida.Size = new System.Drawing.Size(212, 22);
            this.dtpdatasaida.TabIndex = 8;
            this.dtpdatasaida.Value = new System.DateTime(2018, 9, 25, 0, 0, 0, 0);
            this.dtpdatasaida.ValueChanged += new System.EventHandler(this.dtpdatasaida_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 17);
            this.label3.TabIndex = 61;
            this.label3.Text = "CEP";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 229);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 17);
            this.label5.TabIndex = 49;
            this.label5.Text = "Endereço";
            // 
            // txtnumero
            // 
            this.txtnumero.Location = new System.Drawing.Point(357, 249);
            this.txtnumero.Name = "txtnumero";
            this.txtnumero.Size = new System.Drawing.Size(95, 23);
            this.txtnumero.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(126, 272);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 17);
            this.label11.TabIndex = 67;
            this.label11.Text = "Bairro";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(138, 209);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(22, 17);
            this.label12.TabIndex = 68;
            this.label12.Text = "UF";
            // 
            // cbouf
            // 
            this.cbouf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbouf.FormattingEnabled = true;
            this.cbouf.Items.AddRange(new object[] {
            "SP",
            "RJ"});
            this.cbouf.Location = new System.Drawing.Point(166, 205);
            this.cbouf.Name = "cbouf";
            this.cbouf.Size = new System.Drawing.Size(45, 25);
            this.cbouf.TabIndex = 15;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(225, 209);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 17);
            this.label13.TabIndex = 70;
            this.label13.Text = "Cidade";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.txtcelular);
            this.panel4.Controls.Add(this.label26);
            this.panel4.Controls.Add(this.txttelefone);
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.pictureBox3);
            this.panel4.Controls.Add(this.label28);
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Controls.Add(this.pictureBox2);
            this.panel4.Location = new System.Drawing.Point(1, 354);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(649, 112);
            this.panel4.TabIndex = 65;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // txtcelular
            // 
            this.txtcelular.Location = new System.Drawing.Point(227, 28);
            this.txtcelular.Mask = "(00) 0 0000-0000";
            this.txtcelular.Name = "txtcelular";
            this.txtcelular.Size = new System.Drawing.Size(200, 23);
            this.txtcelular.TabIndex = 50;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(584, 71);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(49, 17);
            this.label26.TabIndex = 215;
            this.label26.Text = "Alterar";
            // 
            // txttelefone
            // 
            this.txttelefone.Location = new System.Drawing.Point(6, 28);
            this.txttelefone.Mask = " (00) 0000-0000";
            this.txttelefone.Name = "txttelefone";
            this.txttelefone.Size = new System.Drawing.Size(201, 23);
            this.txttelefone.TabIndex = 49;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(515, 71);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(63, 17);
            this.label27.TabIndex = 214;
            this.label27.Text = "Cancelar";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Buffet.Properties.Resources.alterar;
            this.pictureBox3.Location = new System.Drawing.Point(580, 35);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(58, 33);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 212;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(463, 71);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(46, 17);
            this.label28.TabIndex = 213;
            this.label28.Text = "Salvar";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Buffet.Properties.Resources._1024px_Deletion_icon_svg;
            this.pictureBox1.Location = new System.Drawing.Point(518, 35);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(58, 33);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 210;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Buffet.Properties.Resources._678134_sign_check_512;
            this.pictureBox2.Location = new System.Drawing.Point(456, 35);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(58, 33);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 211;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(5, 138);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 19);
            this.label14.TabIndex = 48;
            this.label14.Text = "Contatos";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(7, 364);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 17);
            this.label15.TabIndex = 71;
            this.label15.Text = "Telefone 1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(14, 8);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 17);
            this.label16.TabIndex = 73;
            this.label16.Text = "Usuário";
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 407);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 17);
            this.label17.TabIndex = 75;
            this.label17.Text = "Email";
            // 
            // txtemail
            // 
            this.txtemail.Location = new System.Drawing.Point(9, 427);
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(421, 23);
            this.txtemail.TabIndex = 24;
            this.txtemail.TextChanged += new System.EventHandler(this.txtemail_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(227, 63);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 17);
            this.label18.TabIndex = 80;
            this.label18.Text = "Observações";
            // 
            // txtobservacaofuncionarios
            // 
            this.txtobservacaofuncionarios.Location = new System.Drawing.Point(228, 82);
            this.txtobservacaofuncionarios.Multiline = true;
            this.txtobservacaofuncionarios.Name = "txtobservacaofuncionarios";
            this.txtobservacaofuncionarios.Size = new System.Drawing.Size(173, 80);
            this.txtobservacaofuncionarios.TabIndex = 9;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(459, 207);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(92, 17);
            this.label19.TabIndex = 82;
            this.label19.Text = "Observações";
            // 
            // txtobservacoesendereco
            // 
            this.txtobservacoesendereco.Location = new System.Drawing.Point(462, 227);
            this.txtobservacoesendereco.Multiline = true;
            this.txtobservacoesendereco.Name = "txtobservacoesendereco";
            this.txtobservacoesendereco.Size = new System.Drawing.Size(173, 85);
            this.txtobservacoesendereco.TabIndex = 21;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.btncep);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Location = new System.Drawing.Point(2, 197);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(648, 164);
            this.panel3.TabIndex = 83;
            // 
            // btncep
            // 
            this.btncep.BackgroundImage = global::Buffet.Properties.Resources.search;
            this.btncep.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btncep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncep.Location = new System.Drawing.Point(111, 10);
            this.btncep.Name = "btncep";
            this.btncep.Size = new System.Drawing.Size(17, 18);
            this.btncep.TabIndex = 49;
            this.btncep.UseVisualStyleBackColor = true;
            this.btncep.Click += new System.EventHandler(this.btncep_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 178);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 19);
            this.label9.TabIndex = 48;
            this.label9.Text = "Endereço";
            // 
            // cbosituacao
            // 
            this.cbosituacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbosituacao.FormattingEnabled = true;
            this.cbosituacao.Items.AddRange(new object[] {
            "Ativo",
            "Inativo"});
            this.cbosituacao.Location = new System.Drawing.Point(423, 82);
            this.cbosituacao.Name = "cbosituacao";
            this.cbosituacao.Size = new System.Drawing.Size(212, 25);
            this.cbosituacao.TabIndex = 5;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(423, 63);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(65, 17);
            this.label22.TabIndex = 84;
            this.label22.Text = "Situação";
            // 
            // txtusuario
            // 
            this.txtusuario.Location = new System.Drawing.Point(14, 29);
            this.txtusuario.Name = "txtusuario";
            this.txtusuario.Size = new System.Drawing.Size(164, 23);
            this.txtusuario.TabIndex = 10;
            // 
            // txtsenha
            // 
            this.txtsenha.Location = new System.Drawing.Point(17, 70);
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.Size = new System.Drawing.Size(164, 23);
            this.txtsenha.TabIndex = 11;
            this.txtsenha.UseSystemPasswordChar = true;
            this.txtsenha.TextChanged += new System.EventHandler(this.txtsenha_TextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(227, 362);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 17);
            this.label23.TabIndex = 90;
            this.label23.Text = "Celular";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(14, 53);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(47, 17);
            this.label24.TabIndex = 91;
            this.label24.Text = "Senha";
            // 
            // txtcpf
            // 
            this.txtcpf.Location = new System.Drawing.Point(315, 29);
            this.txtcpf.Mask = "000,000,000-00";
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.Size = new System.Drawing.Size(96, 23);
            this.txtcpf.TabIndex = 6;
            this.txtcpf.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBox1_MaskInputRejected);
            // 
            // txtcep
            // 
            this.txtcep.Location = new System.Drawing.Point(48, 207);
            this.txtcep.Mask = "00000-000";
            this.txtcep.Name = "txtcep";
            this.txtcep.Size = new System.Drawing.Size(64, 23);
            this.txtcep.TabIndex = 14;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(4, 40);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(660, 502);
            this.tabControl1.TabIndex = 254;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel11);
            this.tabPage1.Controls.Add(this.cbosituacao);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.txtcep);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.txtemail);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.panel4);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.txtnome);
            this.tabPage1.Controls.Add(this.txtobservacoesendereco);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.txtcpf);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.cbouf);
            this.tabPage1.Controls.Add(this.cbocargo);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.txtnumero);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.txtendendereco);
            this.tabPage1.Controls.Add(this.txtbairro);
            this.tabPage1.Controls.Add(this.txtcidade);
            this.tabPage1.Controls.Add(this.txtobservacaofuncionarios);
            this.tabPage1.Controls.Add(this.txtcomplemento);
            this.tabPage1.Controls.Add(this.dtpdatasaida);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(652, 472);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Dados";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel11.Controls.Add(this.label16);
            this.panel11.Controls.Add(this.txtusuario);
            this.panel11.Controls.Add(this.txtsenha);
            this.panel11.Controls.Add(this.label24);
            this.panel11.Location = new System.Drawing.Point(9, 56);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(202, 106);
            this.panel11.TabIndex = 92;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label34);
            this.tabPage2.Controls.Add(this.panel10);
            this.tabPage2.Controls.Add(this.label33);
            this.tabPage2.Controls.Add(this.panel9);
            this.tabPage2.Controls.Add(this.Fornecedor);
            this.tabPage2.Controls.Add(this.panel8);
            this.tabPage2.Controls.Add(this.label32);
            this.tabPage2.Controls.Add(this.panel7);
            this.tabPage2.Controls.Add(this.label31);
            this.tabPage2.Controls.Add(this.panel6);
            this.tabPage2.Controls.Add(this.label29);
            this.tabPage2.Controls.Add(this.panel5);
            this.tabPage2.Controls.Add(this.pictureBox7);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(652, 472);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Permissões";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(14, 360);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(122, 22);
            this.label34.TabIndex = 114;
            this.label34.Text = "Tercerizados";
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.rbtnaotercerizados);
            this.panel10.Controls.Add(this.rbtsimtercerizados);
            this.panel10.Location = new System.Drawing.Point(16, 373);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(151, 47);
            this.panel10.TabIndex = 113;
            // 
            // rbtnaotercerizados
            // 
            this.rbtnaotercerizados.AutoSize = true;
            this.rbtnaotercerizados.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnaotercerizados.Location = new System.Drawing.Point(84, 18);
            this.rbtnaotercerizados.Name = "rbtnaotercerizados";
            this.rbtnaotercerizados.Size = new System.Drawing.Size(61, 26);
            this.rbtnaotercerizados.TabIndex = 99;
            this.rbtnaotercerizados.Text = "Não";
            this.rbtnaotercerizados.UseVisualStyleBackColor = true;
            // 
            // rbtsimtercerizados
            // 
            this.rbtsimtercerizados.AutoSize = true;
            this.rbtsimtercerizados.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtsimtercerizados.Location = new System.Drawing.Point(6, 16);
            this.rbtsimtercerizados.Name = "rbtsimtercerizados";
            this.rbtsimtercerizados.Size = new System.Drawing.Size(58, 26);
            this.rbtsimtercerizados.TabIndex = 102;
            this.rbtsimtercerizados.Text = "Sim";
            this.rbtsimtercerizados.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(10, 296);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(83, 22);
            this.label33.TabIndex = 113;
            this.label33.Text = "Estoque";
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.rbtnaoestoque);
            this.panel9.Controls.Add(this.rbtsimestoque);
            this.panel9.Location = new System.Drawing.Point(14, 310);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(151, 47);
            this.panel9.TabIndex = 112;
            // 
            // rbtnaoestoque
            // 
            this.rbtnaoestoque.AutoSize = true;
            this.rbtnaoestoque.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnaoestoque.Location = new System.Drawing.Point(84, 18);
            this.rbtnaoestoque.Name = "rbtnaoestoque";
            this.rbtnaoestoque.Size = new System.Drawing.Size(61, 26);
            this.rbtnaoestoque.TabIndex = 99;
            this.rbtnaoestoque.Text = "Não";
            this.rbtnaoestoque.UseVisualStyleBackColor = true;
            // 
            // rbtsimestoque
            // 
            this.rbtsimestoque.AutoSize = true;
            this.rbtsimestoque.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtsimestoque.Location = new System.Drawing.Point(3, 16);
            this.rbtsimestoque.Name = "rbtsimestoque";
            this.rbtsimestoque.Size = new System.Drawing.Size(58, 26);
            this.rbtsimestoque.TabIndex = 101;
            this.rbtsimestoque.Text = "Sim";
            this.rbtsimestoque.UseVisualStyleBackColor = true;
            // 
            // Fornecedor
            // 
            this.Fornecedor.AutoSize = true;
            this.Fornecedor.BackColor = System.Drawing.Color.Transparent;
            this.Fornecedor.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fornecedor.Location = new System.Drawing.Point(17, 237);
            this.Fornecedor.Name = "Fornecedor";
            this.Fornecedor.Size = new System.Drawing.Size(116, 22);
            this.Fornecedor.TabIndex = 112;
            this.Fornecedor.Text = "Fornecedor";
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.rbtnaofornecedor);
            this.panel8.Controls.Add(this.rbtsimfornecedor);
            this.panel8.Location = new System.Drawing.Point(14, 246);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(151, 47);
            this.panel8.TabIndex = 111;
            // 
            // rbtnaofornecedor
            // 
            this.rbtnaofornecedor.AutoSize = true;
            this.rbtnaofornecedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnaofornecedor.Location = new System.Drawing.Point(84, 18);
            this.rbtnaofornecedor.Name = "rbtnaofornecedor";
            this.rbtnaofornecedor.Size = new System.Drawing.Size(61, 26);
            this.rbtnaofornecedor.TabIndex = 99;
            this.rbtnaofornecedor.Text = "Não";
            this.rbtnaofornecedor.UseVisualStyleBackColor = true;
            // 
            // rbtsimfornecedor
            // 
            this.rbtsimfornecedor.AutoSize = true;
            this.rbtsimfornecedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtsimfornecedor.Location = new System.Drawing.Point(3, 18);
            this.rbtsimfornecedor.Name = "rbtsimfornecedor";
            this.rbtsimfornecedor.Size = new System.Drawing.Size(58, 26);
            this.rbtsimfornecedor.TabIndex = 100;
            this.rbtsimfornecedor.Text = "Sim";
            this.rbtsimfornecedor.UseVisualStyleBackColor = true;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(14, 166);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(116, 22);
            this.label32.TabIndex = 111;
            this.label32.Text = "Funcionario";
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.rbtnaofuncionario);
            this.panel7.Controls.Add(this.rbtsimfuncionario);
            this.panel7.Location = new System.Drawing.Point(14, 182);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(151, 47);
            this.panel7.TabIndex = 110;
            // 
            // rbtnaofuncionario
            // 
            this.rbtnaofuncionario.AutoSize = true;
            this.rbtnaofuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnaofuncionario.Location = new System.Drawing.Point(84, 18);
            this.rbtnaofuncionario.Name = "rbtnaofuncionario";
            this.rbtnaofuncionario.Size = new System.Drawing.Size(61, 26);
            this.rbtnaofuncionario.TabIndex = 99;
            this.rbtnaofuncionario.Text = "Não";
            this.rbtnaofuncionario.UseVisualStyleBackColor = true;
            // 
            // rbtsimfuncionario
            // 
            this.rbtsimfuncionario.AutoSize = true;
            this.rbtsimfuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtsimfuncionario.Location = new System.Drawing.Point(3, 16);
            this.rbtsimfuncionario.Name = "rbtsimfuncionario";
            this.rbtsimfuncionario.Size = new System.Drawing.Size(58, 26);
            this.rbtsimfuncionario.TabIndex = 99;
            this.rbtsimfuncionario.Text = "Sim";
            this.rbtsimfuncionario.UseVisualStyleBackColor = true;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(14, 108);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(74, 22);
            this.label31.TabIndex = 110;
            this.label31.Text = "Pedido";
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.rbtnaopedido);
            this.panel6.Controls.Add(this.rbtsimpedido);
            this.panel6.Location = new System.Drawing.Point(14, 116);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(151, 47);
            this.panel6.TabIndex = 109;
            // 
            // rbtnaopedido
            // 
            this.rbtnaopedido.AutoSize = true;
            this.rbtnaopedido.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnaopedido.Location = new System.Drawing.Point(84, 18);
            this.rbtnaopedido.Name = "rbtnaopedido";
            this.rbtnaopedido.Size = new System.Drawing.Size(61, 26);
            this.rbtnaopedido.TabIndex = 99;
            this.rbtnaopedido.Text = "Não";
            this.rbtnaopedido.UseVisualStyleBackColor = true;
            // 
            // rbtsimpedido
            // 
            this.rbtsimpedido.AutoSize = true;
            this.rbtsimpedido.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtsimpedido.Location = new System.Drawing.Point(3, 16);
            this.rbtsimpedido.Name = "rbtsimpedido";
            this.rbtsimpedido.Size = new System.Drawing.Size(58, 26);
            this.rbtsimpedido.TabIndex = 98;
            this.rbtsimpedido.Text = "Sim";
            this.rbtsimpedido.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(5, 49);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(74, 22);
            this.label29.TabIndex = 109;
            this.label29.Text = "Cliente";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.rdbnaocliente);
            this.panel5.Controls.Add(this.rdbsimcliente);
            this.panel5.Location = new System.Drawing.Point(14, 55);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(151, 47);
            this.panel5.TabIndex = 108;
            // 
            // rdbnaocliente
            // 
            this.rdbnaocliente.AutoSize = true;
            this.rdbnaocliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbnaocliente.Location = new System.Drawing.Point(84, 18);
            this.rdbnaocliente.Name = "rdbnaocliente";
            this.rdbnaocliente.Size = new System.Drawing.Size(61, 26);
            this.rdbnaocliente.TabIndex = 99;
            this.rdbnaocliente.Text = "Não";
            this.rdbnaocliente.UseVisualStyleBackColor = true;
            // 
            // rdbsimcliente
            // 
            this.rdbsimcliente.AutoSize = true;
            this.rdbsimcliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbsimcliente.Location = new System.Drawing.Point(13, 18);
            this.rdbsimcliente.Name = "rdbsimcliente";
            this.rdbsimcliente.Size = new System.Drawing.Size(58, 26);
            this.rdbsimcliente.TabIndex = 98;
            this.rdbsimcliente.Text = "Sim";
            this.rdbsimcliente.UseVisualStyleBackColor = true;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Buffet.Properties.Resources.locking;
            this.pictureBox7.Location = new System.Drawing.Point(288, 74);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(302, 263);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 106;
            this.pictureBox7.TabStop = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Century Gothic", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(6, 8);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(157, 32);
            this.label30.TabIndex = 96;
            this.label30.Text = "Permissões";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.lbltitulo);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(668, 34);
            this.panel2.TabIndex = 255;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(612, 1);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(24, 27);
            this.label21.TabIndex = 255;
            this.label21.Text = "_";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(642, 5);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 23);
            this.label25.TabIndex = 254;
            this.label25.Text = "X";
            // 
            // lbltitulo
            // 
            this.lbltitulo.AutoSize = true;
            this.lbltitulo.BackColor = System.Drawing.Color.Transparent;
            this.lbltitulo.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltitulo.ForeColor = System.Drawing.Color.White;
            this.lbltitulo.Location = new System.Drawing.Point(5, 5);
            this.lbltitulo.Name = "lbltitulo";
            this.lbltitulo.Size = new System.Drawing.Size(209, 21);
            this.lbltitulo.TabIndex = 49;
            this.lbltitulo.Text = "Cadastro de Funcionários";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(0, 565);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(36, 26);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 297;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Buffet.Properties.Resources.logo1;
            this.pictureBox5.Location = new System.Drawing.Point(296, 548);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(70, 43);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 296;
            this.pictureBox5.TabStop = false;
            // 
            // toolTip1
            // 
            this.toolTip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.toolTip1.ForeColor = System.Drawing.Color.Black;
            // 
            // CadastroFuncionarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(668, 594);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CadastroFuncionarios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CadastroFuncionarios";
            this.Load += new System.EventHandler(this.CadastroFuncionarios_Load);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ComboBox cbocargo;
        private System.Windows.Forms.TextBox txtbairro;
        private System.Windows.Forms.TextBox txtcidade;
        private System.Windows.Forms.TextBox txtcomplemento;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtendendereco;
        private System.Windows.Forms.DateTimePicker dtpdatasaida;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtnumero;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbouf;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtobservacaofuncionarios;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtobservacoesendereco;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbosituacao;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtusuario;
        private System.Windows.Forms.TextBox txtsenha;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.MaskedTextBox txtcpf;
        private System.Windows.Forms.MaskedTextBox txtcep;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.MaskedTextBox txtcelular;
        private System.Windows.Forms.MaskedTextBox txttelefone;
        private System.Windows.Forms.Button btncep;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.RadioButton rbtsimtercerizados;
        private System.Windows.Forms.RadioButton rbtsimestoque;
        private System.Windows.Forms.RadioButton rbtsimfornecedor;
        private System.Windows.Forms.RadioButton rbtsimfuncionario;
        private System.Windows.Forms.RadioButton rbtsimpedido;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton rdbnaocliente;
        private System.Windows.Forms.RadioButton rdbsimcliente;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.RadioButton rbtnaopedido;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.RadioButton rbtnaofuncionario;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.RadioButton rbtnaofornecedor;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.RadioButton rbtnaoestoque;
        private System.Windows.Forms.Label Fornecedor;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.RadioButton rbtnaotercerizados;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lbltitulo;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}