﻿using Buffet.Db.Funcionarios;
using Buffet.Utilitarios;
using Correios;
using House_of_Taste.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class CadastroFuncionarios : Form
    {
        public CadastroFuncionarios()
        {
            InitializeComponent();
            LoadScreen();
           
        }
        private void LoadScreen()
        {
            UF uf = new UF();
            cbouf.DataSource = uf.UFS();
            dtpdatasaida.Enabled = false;
            dtpdatasaida.Visible = false;
            label7.Enabled = false;
            label7.Visible = false;
            label22.Visible = false;
            cbosituacao.Visible = false;
            cbosituacao.Enabled = false;
                
        }
        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {
            Close();

        }


        DTO_Fucionario funcionario = new DTO_Fucionario();

        private void Permissoes()
        {
        //    if (rdnadm.Checked == true)
        //    {
        //        rdbsimcliente.Checked = true;
        //        rbtsimestoque.Checked = true;
        //        rbtsimfornecedor.Checked = true;
        //        rbtsimfuncionario.Checked = true;
        //        rbtsimpedido.Checked = true;
        //        rbtsimtercerizados.Checked = true;             
        //    }
        //    if (rdnsempermissao.Checked == true)
        //    {

        //        rdbnaocliente.Checked = false;
        //        rbtsimestoque.Checked = false;
        //        rbtsimfornecedor.Checked = false;
        //        rbtsimfuncionario.Checked = false;
        //        rbtsimpedido.Checked = false;
        //        rbtsimtercerizados.Checked = false;

        //    }

            //Permissões Cliente
            if (rdbsimcliente.Checked == true)
            {
                funcionario.pr_permissao_cliente = true;
                funcionario.pr_permissao_cadastrar_cliente = true;
                funcionario.pr_permissao_consultar_cliente = true;
                funcionario.pr_permissao_atualiza_cadastror_cliente = true;
            }
            if (rdbnaocliente.Checked == true)
            {
                funcionario.pr_permissao_cliente = false;
                funcionario.pr_permissao_cadastrar_cliente = false;
                funcionario.pr_permissao_consultar_cliente = false;
                funcionario.pr_permissao_atualiza_cadastror_cliente = false;

               
            }

            //Permissões Estoque
            if (rbtsimestoque.Checked == true)
            {
                funcionario.pr_permissao_estoque = true;
                funcionario.pr_permissao_consultar_estoque = true;
                funcionario.pr_permissao_alterar_estoque = true;
            }
            if (rbtnaoestoque.Checked == true)
            {
                funcionario.pr_permissao_estoque = false;
                funcionario.pr_permissao_consultar_estoque = false;
                funcionario.pr_permissao_alterar_estoque = false;

               
            }

            //Permissoes Fornecedor
            if (rbtsimfornecedor.Checked == true)
            {
                funcionario.pr_permissao_fornecedor = true;
                funcionario.pr_permissao_cadastrar_fornecedor = true;
                funcionario.pr_permissao_consultar_fornecedor = true;
                funcionario.pr_permissao_perdir_ao_fornecedor = true;
            }
            if (rbtnaofornecedor.Checked == true)
            {
                funcionario.pr_permissao_fornecedor = false;
                funcionario.pr_permissao_cadastrar_fornecedor = false;
                funcionario.pr_permissao_consultar_fornecedor = false;
                funcionario.pr_permissao_perdir_ao_fornecedor = false;

               
            }

            //Permissões Funcionario
            if (rbtsimfuncionario.Checked == true)
            {
                funcionario.pr_permissao_funcionario = true;
                funcionario.pr_permissao_cadastrar_funcionario = true;
                funcionario.pr_permissao_novo_funcionario = true;
                funcionario.pr_permissao_alterar_dados_funcionarios = true;
                funcionario.pr_permissao_consultar_funcionarios = true;
            }
            if (rbtnaofuncionario.Checked == true)
            {
                funcionario.pr_permissao_funcionario = false;
                funcionario.pr_permissao_cadastrar_funcionario = false;
                funcionario.pr_permissao_novo_funcionario = false;
                funcionario.pr_permissao_alterar_dados_funcionarios = false;
                funcionario.pr_permissao_consultar_funcionarios = false;

               
            }

            //Permissoes Pedido
            if (rbtsimpedido.Checked == true)
            {
                funcionario.pr_permissao_pedido = true;
                funcionario.pr_permissao_novo_pedido = true;
                funcionario.pr_permissao_atualizar_pedido = true;
                funcionario.pr_permissao_consultar_pedido = true;
            }
            if (rbtnaopedido.Checked == true)
            {
                funcionario.pr_permissao_pedido = false;
                funcionario.pr_permissao_novo_pedido = false;
                funcionario.pr_permissao_atualizar_pedido = false;
                funcionario.pr_permissao_consultar_pedido = false;

               
            }

            //Permissoes Tercerizados
            if (rbtsimtercerizados.Checked == true)
            {
                funcionario.pr_permissao_cadastrar_terceiriziados = true;
                funcionario.pr_permissao_consultar_terceriziado = true;
                funcionario.pr_permisssao_terceirizados = true;
            }
            if (rbtnaotercerizados.Checked == true)
            {
                funcionario.pr_permissao_cadastrar_terceiriziados = false;
                funcionario.pr_permissao_consultar_terceriziado = false;
                funcionario.pr_permisssao_terceirizados = false;

               
            }
        }

      
        private void EnviarConfirmacao()
        {
            string para = txtemail.Text;
            string copiaoculta = "smarti7empresa@gmail.com";
            string copia = "smarti7empresa@gmail.com";
            string assunto = "Cadastro Magic Buffet";
            bool comhtml = false;

            string nomeuser = txtnome.Text;
            string cargouser = cbocargo.SelectedItem.ToString();

            string mensagem =
               @"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>


<html xmlns='http://www.w3.org/1999/xhtml'>

 <head>
 	<!--Import Google Icon Font-->
      <link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'>
      <!--Import materialize.css-->
      <link type='text/css' rel='stylesheet' href='css/materialize.min.css'  media='screen,projection'/>

  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
  <title>Magic Buffet</title>
  <meta name='viewport' content='width=device-width, initial-scale=1.0'/>
</head>


<body style='margin: 0; padding: 0;'>


 <table align='center' border='0' cellpadding='0' cellspacing='0' width='600'>
 <tr>
  <td align='center' bgcolor='#70bbd9' style='padding: 40px 0 30px 0;'>
 <img src='https://www.catho.com.br/carreira-sucesso/wp-content/uploads/sites/3/2018/01/boas-vindas.jpg' alt='Magic Buffet' width='300' height='230' style='display: block;' />
</td>
 </tr>

 <tr>
  <td bgcolor='#ffffff' style='padding: 40px 30px 40px 30px;'>


 <table border='0' cellpadding='0' cellspacing='0' width='100%'>
 <tr>

  <td>
   <h1>Magic Buffet</h1>
  </td>
 </tr>
 <tr>
  <td style='padding: 20px 0 30px 0;'>
       'Olá, informamos que '
                {nomeuser}
                 acabou de ser cadasrtrado no sistema  
                ERD da empresa Magic Buffet
                ocupando o cargo de  
              {cargouser}
               Caso não seja voce, por favor entrar em contato com a empresa.  Magic Buffet
  </td>
 </tr>

 </table>


</td>
 </tr>
 <tr>
  <td bgcolor='#ee4c50' style='padding: 30px 30px 30px 30px;'>
 <table border='0' cellpadding='0' cellspacing='0' width='100%'>
 <tr>
  <td align='right'>
 <table border='0' cellpadding='0' cellspacing='0'>
  <tr>
   <td>
    <a href='http://www.twitter.com/'>
     <img src='https://cdn.icon-icons.com/icons2/838/PNG/512/circle-twitter_icon-icons.com_66835.png' alt='Twitter' width='38' height='38' style='display: block;' border='0' />
    </a>
   </td>
   <td style='font-size: 0; line-height: 0;' width='20'>&nbsp;</td>
   <td>
    <a href='http://www.twitter.com/'>
     <img src='https://png.pngtree.com/element_our/md/20180301/md_5a9794da1b10e.png' alt='Facebook' width='38' height='38' style='display: block;' border='0' />
    </a>
   </td>
  </tr>
 </table>
</td>
 </tr>
</table>
</td>
 </tr>
</table>


</body>


</html>

<style '>
@import url('https://fonts.googleapis.com/css?family=Dancing+Script');
+

</style>";
                

            Email email = new Email();
            email.EnviarGmail(para, copia, copiaoculta, assunto, mensagem, comhtml);
           
        }

        private void button1_Click(object sender, EventArgs e)
        {

          
            funcionario.Nome = txtnome.Text;
            funcionario.CPF = txtcpf.Text;
            funcionario.Data_Cadastro = DateTime.Now;
            funcionario.Situacao = cbosituacao.SelectedItem.ToString();
            //funcionario.Data_Saida = dtpdatasaida.Value;
            funcionario.cargo = cbocargo.SelectedItem.ToString();
            funcionario.observacao_Cadastro = txtobservacaofuncionarios.Text;
            funcionario.CEP = txtcep.Text;
            funcionario.UF = cbouf.SelectedItem.ToString();
            funcionario.cidade = txtcidade.Text;
            funcionario.endereco = txtendendereco.Text;
            funcionario.complemento = txtcomplemento.Text;
            funcionario.bairro = txtbairro.Text;
            funcionario.Numero = txtnumero.Text;
            funcionario.observacoes_Endereco = txtobservacoesendereco.Text;
            funcionario.telefone = txttelefone.Text;
            funcionario.celular = txtcelular.Text;
            funcionario.email = txtemail.Text;
            funcionario.user_usuario = txtusuario.Text;
            funcionario.sn_senha = txtsenha.Text;

            Permissoes();

            Business_Funcionario business = new Business_Funcionario();
            business.Salvar(funcionario);

            MessageBox.Show("Funcionário cadastrado com sucesso!", 
                            "Magic Buffet",
                            MessageBoxButtons.OK, 
                            MessageBoxIcon.Information);

            EnviarConfirmacao();
           

        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Funcionário alterado com sucesso!", 
                            "Magic Buffet",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
            Hide();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label25_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void label21_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        //Botão de Salvar
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtnome.Text == string.Empty)
                    MessageBox.Show("O campo nome não pode estar em branco.");
                if (txtusuario.Text == string.Empty)
                    MessageBox.Show("O campo usuário não pode estar em branco.");
                if (txtsenha.Text == string.Empty)
                    MessageBox.Show("O campo senha não pode estar em branco.");
                if (txtcep.Text == string.Empty)
                    MessageBox.Show("O campo de CEP não pode estar em branco.");
                if (txtnumero.Text == string.Empty)
                    MessageBox.Show("O campo numero não pode estar em branco.");
                if (txtcelular.Text == string.Empty)
                    MessageBox.Show("O campo do numero do celular não pode estar em branco.");
                if (txtemail.Text == string.Empty)
                    MessageBox.Show("Pr favor, digite um endereço de E-mail válido.");
                //if (rdbsimcliente.Checked || rdbnaocliente.Checked == false)
                //    MessageBox.Show("Marque uma permissão para controle de cliente.");
                //if (rbtsimpedido.Checked || rbtnaopedido.Checked == false)
                //    MessageBox.Show("Marque uma permissão para controle de pedidos.");
                //if (rbtsimfuncionario.Checked || rbtnaofuncionario.Checked == false)
                //    MessageBox.Show("Marque uma permissão para controle de funcionários.");
                //if (rbtsimfornecedor.Checked || rbtnaofornecedor.Checked == false)
                //    MessageBox.Show("Marque uma permissão para controle de fornecedores.");
                //if (rbtsimestoque.Checked || rbtnaoestoque.Checked == false)
                //    MessageBox.Show("Marque uma permissão para controle de estoque.");
                //if (rbtsimtercerizados.Checked || rbtnaotercerizados.Checked == false)
                //    MessageBox.Show("Marque uma permissão para controle de terceirizados.");

                DTO_Fucionario funcionario = new DTO_Fucionario();

                funcionario.Nome = txtnome.Text;
                funcionario.CPF = txtcpf.Text;
                funcionario.Data_Cadastro = DateTime.Now;
                funcionario.Situacao = "Ativo";
                //funcionario.Data_Saida = dtpdatasaida.Value;
                funcionario.cargo = cbocargo.SelectedItem.ToString();
                funcionario.observacao_Cadastro = txtobservacaofuncionarios.Text;
                funcionario.CEP = txtcep.Text;
                funcionario.UF = cbouf.SelectedItem.ToString();
                funcionario.cidade = txtcidade.Text;
                funcionario.endereco = txtendendereco.Text;
                funcionario.complemento = txtcomplemento.Text;
                funcionario.bairro = txtbairro.Text;
                funcionario.Numero = txtnumero.Text;
                funcionario.observacoes_Endereco = txtobservacoesendereco.Text;
                funcionario.telefone = txttelefone.Text;
                funcionario.celular = txtcelular.Text;
                funcionario.email = txtemail.Text;
                funcionario.user_usuario = txtusuario.Text;
                funcionario.sn_senha = txtsenha.Text;

                Permissoes();

                Business_Funcionario business = new Business_Funcionario();
                business.Salvar(funcionario);

                MessageBox.Show("Funcionário cadastrado com sucesso!",
                                "Magic Buffet",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);

                EnviarConfirmacao();

            }
            catch 
            {
                MessageBox.Show("Algo de errado não está certo!",
                                "Magic Buffet",
                                MessageBoxButtons.AbortRetryIgnore,
                                MessageBoxIcon.Error);


            }

        }

        public void LoadScreenAlterar(DTO_Fucionario dto)
        {

        }

        //Botão de alterar
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Dados alterados com sucesso!",
                            "Magic Buffet",
                            MessageBoxButtons.OK, 
                            MessageBoxIcon.Information);
            Hide();
        }

        //Botão Cancelar
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void dtpdatasaida_ValueChanged(object sender, EventArgs e)
        {

        }

        private void CadastroFuncionarios_Load(object sender, EventArgs e)
        {

        }


        private void btncep_Click(object sender, EventArgs e)
        {
            ProcurarCEP();          
        }

        
           private void ProcurarCEP()
        {
            try
            {

                string cep = txtcep.Text;
                var service = new CorreiosApi();
                var dados = service.consultaCEP(cep);
                txtcidade.Text = dados.cidade;
                txtendendereco.Text = dados.end;
                txtbairro.Text = dados.bairro;
                cbouf.SelectedItem = dados.uf;

            }
            catch (Exception)
            {

                MessageBox.Show("CEP não encontrado. Certifique-se de que o número informado está correto. ", "Magic Buffet", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
            }


        
    }

        private void txtemail_TextChanged(object sender, EventArgs e)
        {
            string email = txtemail.Text;

            Regex rg = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");

            bool validou = rg.IsMatch(txtemail.Text);
            if (validou == true)
            {
                txtemail.BackColor = Color.LawnGreen;
            }
            else
            {
                txtemail.BackColor = Color.IndianRed;
            }
        }

        private void cbocargo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void txtsenha_TextChanged(object sender, EventArgs e)
        {
            toolTip1.Show("A senha deve conter letras minúsculas e maiúsculas, números e carateres especias.", txtsenha);

            string senha = txtsenha.Text;
            string ex = @"^.*(?=.{5,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$";
            Regex re = new Regex(ex);
            bool validar = re.IsMatch(txtsenha.Text);
            if (validar == true)
            {
                txtsenha.BackColor = Color.LawnGreen;
            }
            else
            {
                txtsenha.BackColor = Color.IndianRed;
            }
        }

        private void txtnome_TextChanged(object sender, EventArgs e)
        {
                  
        }
    }
}
