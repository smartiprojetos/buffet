﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas.Fluxo_de_Caixa
{
    public partial class FluxoCaixaConsulta : Form
    {
        public FluxoCaixaConsulta()
        {
            InitializeComponent();
        }

        private void label21_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label25_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
