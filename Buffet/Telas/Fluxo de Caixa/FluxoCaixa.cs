﻿using Buffet.Db.FluxodeCaixa;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas.Fluxo_de_Caixa
{
    public partial class FluxoCaixa : Form
    {
        public FluxoCaixa()
        {
            InitializeComponent();
            loadFluxoCaixa(dtkinicio.Value.Date, dtpfim.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(50));
        }

        private void label25_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void loadFluxoCaixa(DateTime start, DateTime end)
        {
            FluxodeCaixaBusiness nha = new FluxodeCaixaBusiness();
            List<FluxodeCaixaView> item = nha.FiltrarData(start, end);
            dgvfluxo.DataSource = item;
        }

        private void label21_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void FluxoCaixa_Load(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void dtkinicio_ValueChanged(object sender, EventArgs e)
        {
            loadFluxoCaixa(dtkinicio.Value.Date, dtpfim.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(50));
        }

        private void dtpfim_ValueChanged(object sender, EventArgs e)
        {
            loadFluxoCaixa(dtkinicio.Value.Date, dtpfim.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(50));
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
