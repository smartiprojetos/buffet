﻿using Buffet.Db.Terceirizados;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class CadastroTerceirizados : Form
    {
        public CadastroTerceirizados()
        {
            InitializeComponent();
            LoadScreen();
        }


        private void label21_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            //Nesse caso, como não precisamos da Chave Primária, não precisamos utilizar a variável de objeto de esopo, e sim, instanciar a nossa
            DTO_Terceirizados dto = new DTO_Terceirizados();

            //Passando o valor do DTO para o método
            SalvarValores(dto);

            //Esquencendo de chamar o método da BUSINESS
            Business_Terceirizados db = new Business_Terceirizados();
            db.Salvar(dto);

            MessageBox.Show("Cadastro efetuado com sucesso!",
                            "Magic Buffet",
                            MessageBoxButtons.OK, 
                            MessageBoxIcon.Information);
            Hide();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Business_Terceirizados bus = new Business_Terceirizados();
            SalvarValores(dto);
            bus.Alterar(dto);

            MessageBox.Show("Alterado com sucesso!", 
                            "Magic Buffet", 
                            MessageBoxButtons.OK, 
                            MessageBoxIcon.Information);
            Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Deseja cancelar o cadastro",
                            "Magic Buffet",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Information);
            Hide();
        }

        //Novamente... Para o método de ALTERAR funcionar, deve-se colocar este DTO como escolo não declarado
        //Ou seja, sem nenhum valor, para que assim, o mesmo consiga pegar o valor que irá ser passada da GRIEDVIEW
        //Caso contrário, quando você instância um objeto, ele já pega os valores contidos no própria objeto
        DTO_Terceirizados dto;

        private void SalvarValores(DTO_Terceirizados dto)
        {         
            dto.celular = txtcelular.Text;
            dto.CPF_CNPJ = txtcpf.Text;
            dto.email = txtemail.Text;
            dto.nome_Fantasia = txtfantasiaapelido.Text;
            dto.observacoes = txtobservacoes.Text;
            dto.telefone = txttelefone.Text;
            dto.Situacao = "Ativo";
            dto.Data_Cadastro = DateTime.Now;
            //dto.Data_Saida =  dtpdatasaida.;
        }

        public void LoadScreenALterar(DTO_Terceirizados dto)
        {
            //Objeto de escopo. Se você passar o cursor do mouse por cima, irá perceber que o mesmo pega o valor da
            //Variável criada lá à acima e não a que está de paramêtro.
            //Por que isso?
            //Porque o dto que estará vindo de paramêtro é o que vem da Gried. Contendo todos os valores de um determinado registro (Linha)
            //Com isso, declaramos o nosso objeto de escopo, e o mesmo irá receber os valores que vinheram como paramêtro do MÉTODO LOADSCREEN
            //Pois dessa forma, iremos ter o valor do ID que será utilizado para fazer a alteração dos dados. Uffa!
            this.dto = dto;

            txtcelular.Text = dto.celular;
            txtcpf.Text = dto.CPF_CNPJ;
            txtemail.Text = dto.email;
            txtfantasiaapelido.Text = dto.nome_Fantasia;
            txtobservacoes.Text = dto.observacoes;
            txttelefone.Text = dto.telefone;
            cbostituacao.SelectedItem = dto.Situacao;
            //dtpdatacadastro.Value = dto.Data_Cadastro;
            //dtpdatasaida.Value = dto.Data_Saida;

            btnsalvar.Visible = false;
            btnsalvar.Enabled = false;
            lblsalvar.Visible = false;

            btnalterar.Visible = true;
            btnalterar.Enabled = true;
            lblalterar.Visible = true;

            dtpdatasaida.Enabled = true;
            dtpdatasaida.Visible = true;
            label7.Enabled = true;
            label7.Visible = true;

            label10.Enabled = true;
            label10.Visible = true;
            cbostituacao.Enabled = true;
            cbostituacao.Visible = true;

        }

        private void CadastroTerceirizados_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            Hide();
        }

        private void LoadScreen()
        {
            btnalterar.Visible = false;
            btnalterar.Enabled = false;
            lblalterar.Visible = false;
            dtpdatasaida.Enabled = false;
            dtpdatasaida.Visible = false;
            label7.Enabled = false;
            label7.Visible = false;

            label10.Enabled = false;
            label10.Visible = false;
            cbostituacao.Enabled = false;
            cbostituacao.Visible = false;
        }

        private void txtemail_TextChanged(object sender, EventArgs e)
        {
            string email = txtemail.Text;

            Regex rg = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");

            bool validou = rg.IsMatch(txtemail.Text);
            if (validou == true)
            {
                txtemail.BackColor = Color.LawnGreen;
            }
            else
            {
                txtemail.BackColor = Color.IndianRed;
            }
        }

        private void rdnfisica_CheckedChanged(object sender, EventArgs e)
        {
            if (rdnfisica.Checked == true)
            {
                lblcpf.Text = "CPF";

                const string mascaraCPF = "000,000,000-00";
                txtcpf.Mask = mascaraCPF;

               

            }
        }

        private void rdnjuridica_CheckedChanged(object sender, EventArgs e)
        {
            if (rdnjuridica.Checked == true)
            {

                lblcpf.Text = "CNPJ";
                const string mascaraCNPJ = "00,000,000/0000-00";
                txtcpf.Mask = mascaraCNPJ;

                //txtCNPJ.Visible = true;
                //txtCNPJ.Enabled = true;

                //txtcpf.Visible = false;
                //txtcpf.Enabled = false;

            }
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
