﻿using Buffet.Db.Estoque;
using Buffet.Db.Fornecedores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class Estoque : Form
    {
        public Estoque()
        {
            InitializeComponent();
            Carregar();
        }

        private void button5_Click (object sender, EventArgs e)
        {
            Telas.EditarEstoque poi = new Telas.EditarEstoque();
            poi.Show();
            Hide();
        }

        private void button7_Click(object sender, EventArgs e)
        {
           
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
        }

        void Carregar()
        {
            Business_Fornecedores business = new Business_Fornecedores();
            List<DTO_Fornecedores> lista = business.Listar();

            cbofornecedores.ValueMember = nameof(DTO_Fornecedores.ID_Fornecedor);
            cbofornecedores.DisplayMember = nameof(DTO_Fornecedores.Nome_Fantasia);
            cbofornecedores.DataSource = lista;
        }

        private void CarregarCombos()
        {

            Business_Estoque bus = new Business_Estoque();
            List<DTO_Estoque> lista = bus.Listar(cbofornecedores.SelectedItem.ToString());
            dgvEstoque.AutoGenerateColumns = false;
            dgvEstoque.DataSource = false;
        }

        private void dgvEstoque_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Database_Estoque db = new Database_Estoque();
            if (e.ColumnIndex == 9)
            {
                DTO_Estoque dto = dgvEstoque.CurrentRow.DataBoundItem as DTO_Estoque;                
                db.Remover(dto);
                CarregarCombos();
                
            }
            if (e.ColumnIndex == 10)
            {
                DTO_Estoque dto = dgvEstoque.CurrentRow.DataBoundItem as DTO_Estoque;
                EditarEstoque tela = new EditarEstoque();

                DTO_Estoque alt = new DTO_Estoque();
                tela.LoadScreenAlterar(alt);
                tela.Show();
                this.Hide();


            }
        }

        private void btnpesquisar_Click(object sender, EventArgs e)
        {
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

            CarregarCombos();

        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Telas.ConsultarFornecedores pe = new Telas.ConsultarFornecedores();
            pe.Show();
            Hide();
        }
    }
}
