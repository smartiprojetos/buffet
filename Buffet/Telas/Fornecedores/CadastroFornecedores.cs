﻿using Buffet.Db.Fornecedores;
using Buffet.Db.Funcionarios;
using Buffet.Utilitarios;
using Correios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class CadastroFornecedores : Form
    {
        public CadastroFornecedores()
        {
            InitializeComponent();
            LoadScreen();
    
        }

        private void LoadScreen()
        {
            UF uf = new UF();
            cboUF.DataSource = uf.UFS();

            btnalterar.Enabled = false;
            btnalterar.Visible = false;
            lblalterar.Visible = false;
            dtpdatasaida.Enabled = false;
            dtpdatasaida.Visible = false;
            label7.Enabled = false;
            label7.Visible = false;
            label10.Enabled = false;
            label10.Visible = false;
            cbosituacao.Enabled = false;
            cbosituacao.Visible = false;

        }


        private void label21_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {


        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtNFor.Text == string.Empty)
                    MessageBox.Show("O campo nome não pode estar em branco.");
                if (txtcpf.Text == string.Empty)
                    MessageBox.Show("Digite um CPF ou CNPJ válido, de acordo com o tipo de pessoa escolhido.");
                if (txtCEP.Text == string.Empty)
                    MessageBox.Show("O campo de CEP não pode estar em branco.");
                if (txtnumero.Text == string.Empty)
                    MessageBox.Show("O campo numero não pode estar em branco.");
                if (txtCelular.Text == string.Empty)
                    MessageBox.Show("O campo do numero do celular não pode estar em branco.");
                if (txtEmail.Text == string.Empty)
                    MessageBox.Show("Pr favor, digite um endereço de E-mail válido.");

                if (cboUF.SelectedIndex <= 0 )
                {
                    MessageBox.Show("O campo UF não pode ficar vazio.");
                }
                else
                {
                    DTO_Fornecedores fornecedor = new DTO_Fornecedores();




                    fornecedor.Nome_Fantasia = txtNFor.Text;

                    fornecedor.Data_Cadastro = DateTime.Now;
                    //fornecedor.Data_Saida = dtpdatasaida.Value;
                    fornecedor.Situacao = "Ativo";
                    fornecedor.CPF_CNPJ = txtcpf.Text;
                    fornecedor.Data_Saida = dtpdatasaida.Value;
                    fornecedor.observacao_Cadastro = txtobsCadastro.Text;
                    fornecedor.CEP = txtCEP.Text;
                    fornecedor.UF = cboUF.SelectedItem.ToString();
                    fornecedor.Cidade = txtCidade.Text;
                    fornecedor.Endereco = txtEndereço.Text;
                    fornecedor.Complemento = txtcomplemento.Text;
                    fornecedor.Bairro = txtBairro.Text;
                    fornecedor.Numero = txtnumero.Text;
                    fornecedor.Observacoes_Endereco = txtObsEndereco.Text;
                    fornecedor.Telefone = txtTelefone.Text;
                    fornecedor.Celular = txtCelular.Text;
                    fornecedor.Email = txtEmail.Text;

                    UF uf = new UF();
                    cboUF.DataSource = uf.UFS();

                    Business_Fornecedores business_Fornecedores = new Business_Fornecedores();
                    business_Fornecedores.Salvar(fornecedor);

                    MessageBox.Show("Fornecedor Cadastrado Com Sucesso!",
                                    "Magic Buffet",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);



                    Hide();

                }
            }
            catch 
            {

                MessageBox.Show("Algo de errado não está certo!",
                                "Magic Buffet",
                                MessageBoxButtons.AbortRetryIgnore,
                                MessageBoxIcon.Error);

            }




        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Hide();
        }

        private void label25_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void label21_Click_1(object sender, EventArgs e)
        {
           this.WindowState = FormWindowState.Minimized;
        }
        DTO_Fornecedores dto;

        public void LoadScreenAlterar(DTO_Fornecedores dto)
        {
            this.dto = dto;
            //lblID.Text = dto.ID_Fornecedor.ToString();
            txtNFor.Text = dto.Nome_Fantasia;
            txtcpf.Text = dto.CPF_CNPJ;
            //dtpdatacadastro.Value = dto.Data_Cadastro;
            dtpdatasaida.Value = dto.Data_Saida;
            cbosituacao.SelectedItem = dto.Situacao;

            txtobsCadastro.Text = dto.observacao_Cadastro;
            txtCEP.Text = dto.CEP;
            cboUF.SelectedItem = dto.UF;
            txtCidade.Text = dto.Cidade;
            txtEndereço.Text = dto.Endereco;
            txtcomplemento.Text = dto.Complemento;
            txtBairro.Text = dto.Bairro;
            txtnumero.Text = dto.Numero;
            txtObsEndereco.Text = dto.Observacoes_Endereco;
            txtTelefone.Text = dto.Telefone;
            txtCelular.Text = dto.Celular;
            txtEmail.Text = dto.Email;

            btnalterar.Enabled = true;
            btnalterar.Visible = true;
            lblalterar.Visible = true;

            btnsalvar.Enabled = false;
            btnsalvar.Visible = false;
            lblsalvar.Visible = false;

            dtpdatasaida.Enabled = true;
            dtpdatasaida.Visible = true;
            label7.Enabled = true;
            label7.Visible = true;

            label10.Enabled = true;
            label10.Visible = true;
            cbosituacao.Enabled = true;
            cbosituacao.Visible = true;

            lbltitulo.Text = "Alterar Fornecedor";
        }
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            dto.Nome_Fantasia = txtNFor.Text;
            dto.CPF_CNPJ = txtcpf.Text;
            dto.Data_Cadastro = DateTime.Now;
            //dto.Data_Saida = dtpdatasaida.Value;
            dto.Situacao = "Ativo";
            dto.Data_Saida = dtpdatasaida.Value;
            dto.observacao_Cadastro = txtobsCadastro.Text;
            dto.CEP = txtCEP.Text;
            dto.UF = cboUF.SelectedItem.ToString();
            dto.Cidade = txtCidade.Text;
            dto.Endereco = txtEndereço.Text;
            dto.Complemento = txtcomplemento.Text;
            dto.Bairro = txtBairro.Text;
            dto.Numero = txtnumero.Text;
            dto.Observacoes_Endereco = txtObsEndereco.Text;
            dto.Telefone = txtTelefone.Text;
            dto.Celular = txtCelular.Text;
            dto.Email = txtEmail.Text;

            Business_Fornecedores business_Fornecedores = new Business_Fornecedores();
            business_Fornecedores.Alterar(dto);
            MessageBox.Show("Fornecedor Alterado com sucesso!",
                            "Magic Buffet",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
            Hide();
        }

        private void CadastroFornecedores_Load(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void maskedTextBox2_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        
            private void ProcurarCEP()
        {
            try
            {
                string cep = txtCEP.Text;
                var service = new CorreiosApi();
                var dados = service.consultaCEP(cep);
                txtCidade.Text = dados.cidade;
                txtEndereço.Text = dados.end;
                txtBairro.Text = dados.bairro;
                cboUF.SelectedItem = dados.uf;



            }
            catch (Exception)
            {
                MessageBox.Show("CEP não encontrado.", "magic Buffet", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
            }

        
    }

        private void btncep_Click(object sender, EventArgs e)
        {
            ProcurarCEP();
        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {
            string email = txtEmail.Text;

            Regex rg = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");

            bool validou = rg.IsMatch(txtEmail.Text);
            if (validou == true)
            {
                txtEmail.BackColor = Color.LawnGreen;
            }
            else
            {
                txtEmail.BackColor = Color.IndianRed;
            }
        }

        private void txtobsCadastro_TextChanged(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void txtcpf_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            if (rdnfisica.Checked == true)
            {
                lblcpf.Text = "CPF";

                const string mascaraCPF = "000,000,000-00";
                txtcpf.Mask = mascaraCPF;

                //txtCNPJ.Visible = false;
                //txtCNPJ.Enabled = false;

                //txtcpf.Visible = true;
                //txtcpf.Enabled = true;

            }
            if (rdnjuridica.Checked == true)
            {

                lblcpf.Text = "CNPJ";
                const string mascaraCNPJ = "00,000,000/0000-00";
                txtcpf.Mask = mascaraCNPJ;

                //txtCNPJ.Visible = true;
                //txtCNPJ.Enabled = true;

                //txtcpf.Visible = false;
                //txtcpf.Enabled = false;

            }
        }

        private void rdnfisica_CheckedChanged(object sender, EventArgs e)
        {
            if (rdnfisica.Checked == true)
            {
                lblcpf.Text = "CPF";

                const string mascaraCPF = "000,000,000-00";
                txtcpf.Mask = mascaraCPF;

                
            }
            
        }

        private void rdnjuridica_CheckedChanged(object sender, EventArgs e)
        {
            if (rdnjuridica.Checked == true)
            {

                lblcpf.Text = "CNPJ";
                const string mascaraCNPJ = "00,000,000/0000-00";
                txtcpf.Mask = mascaraCNPJ;

               

            }
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
