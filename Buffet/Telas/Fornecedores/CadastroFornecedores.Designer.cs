﻿namespace Buffet.Telas
{
    partial class CadastroFornecedores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CadastroFornecedores));
            this.panel2 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lbltitulo = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtobsCadastro = new System.Windows.Forms.TextBox();
            this.dtpdatasaida = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.cbosituacao = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblcpf = new System.Windows.Forms.Label();
            this.txtcpf = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtObsEndereco = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cboUF = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtnumero = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCEP = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEndereço = new System.Windows.Forms.TextBox();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.txtcomplemento = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtCelular = new System.Windows.Forms.MaskedTextBox();
            this.lblalterar = new System.Windows.Forms.Label();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.btnalterar = new System.Windows.Forms.PictureBox();
            this.lblsalvar = new System.Windows.Forms.Label();
            this.btncancelar = new System.Windows.Forms.PictureBox();
            this.btnsalvar = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btncep = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNFor = new System.Windows.Forms.TextBox();
            this.rdnjuridica = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rdnfisica = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnalterar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btncancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnsalvar)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.lbltitulo);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(643, 34);
            this.panel2.TabIndex = 64;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(587, -1);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(24, 27);
            this.label21.TabIndex = 255;
            this.label21.Text = "_";
            this.label21.Click += new System.EventHandler(this.label21_Click_1);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(617, 3);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 23);
            this.label25.TabIndex = 254;
            this.label25.Text = "X";
            this.label25.Click += new System.EventHandler(this.label25_Click);
            // 
            // lbltitulo
            // 
            this.lbltitulo.AutoSize = true;
            this.lbltitulo.BackColor = System.Drawing.Color.Transparent;
            this.lbltitulo.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltitulo.ForeColor = System.Drawing.Color.White;
            this.lbltitulo.Location = new System.Drawing.Point(5, 5);
            this.lbltitulo.Name = "lbltitulo";
            this.lbltitulo.Size = new System.Drawing.Size(202, 21);
            this.lbltitulo.TabIndex = 49;
            this.lbltitulo.Text = "Cadastro de Fornecedor";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(262, 38);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 17);
            this.label18.TabIndex = 93;
            this.label18.Text = "Observações";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // txtobsCadastro
            // 
            this.txtobsCadastro.Location = new System.Drawing.Point(265, 58);
            this.txtobsCadastro.Multiline = true;
            this.txtobsCadastro.Name = "txtobsCadastro";
            this.txtobsCadastro.Size = new System.Drawing.Size(228, 38);
            this.txtobsCadastro.TabIndex = 92;
            this.txtobsCadastro.TextChanged += new System.EventHandler(this.txtobsCadastro_TextChanged);
            // 
            // dtpdatasaida
            // 
            this.dtpdatasaida.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpdatasaida.Location = new System.Drawing.Point(265, 123);
            this.dtpdatasaida.MaxDate = new System.DateTime(2018, 11, 7, 0, 0, 0, 0);
            this.dtpdatasaida.MinDate = new System.DateTime(2018, 11, 7, 0, 0, 0, 0);
            this.dtpdatasaida.Name = "dtpdatasaida";
            this.dtpdatasaida.Size = new System.Drawing.Size(285, 22);
            this.dtpdatasaida.TabIndex = 91;
            this.dtpdatasaida.Value = new System.DateTime(2018, 11, 7, 0, 0, 0, 0);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(265, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 17);
            this.label7.TabIndex = 90;
            this.label7.Text = "Data Saída";
            // 
            // cbosituacao
            // 
            this.cbosituacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbosituacao.FormattingEnabled = true;
            this.cbosituacao.Items.AddRange(new object[] {
            "Ativo",
            "Inativo"});
            this.cbosituacao.Location = new System.Drawing.Point(499, 58);
            this.cbosituacao.Name = "cbosituacao";
            this.cbosituacao.Size = new System.Drawing.Size(115, 21);
            this.cbosituacao.TabIndex = 87;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(499, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 17);
            this.label10.TabIndex = 86;
            this.label10.Text = "Situação";
            // 
            // lblcpf
            // 
            this.lblcpf.AutoSize = true;
            this.lblcpf.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcpf.Location = new System.Drawing.Point(118, 105);
            this.lblcpf.Name = "lblcpf";
            this.lblcpf.Size = new System.Drawing.Size(45, 17);
            this.lblcpf.TabIndex = 84;
            this.lblcpf.Text = "C.P.F.";
            // 
            // txtcpf
            // 
            this.txtcpf.Location = new System.Drawing.Point(121, 125);
            this.txtcpf.Mask = "000.000.000-00";
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.Size = new System.Drawing.Size(138, 20);
            this.txtcpf.TabIndex = 83;
            this.txtcpf.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtcpf_MaskInputRejected);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 17);
            this.label1.TabIndex = 81;
            this.label1.Text = "Fornecedor";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(458, 194);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(92, 17);
            this.label19.TabIndex = 109;
            this.label19.Text = "Observações";
            // 
            // txtObsEndereco
            // 
            this.txtObsEndereco.Location = new System.Drawing.Point(460, 214);
            this.txtObsEndereco.Multiline = true;
            this.txtObsEndereco.Name = "txtObsEndereco";
            this.txtObsEndereco.Size = new System.Drawing.Size(173, 85);
            this.txtObsEndereco.TabIndex = 108;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(224, 196);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 17);
            this.label13.TabIndex = 107;
            this.label13.Text = "Cidade";
            // 
            // cboUF
            // 
            this.cboUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboUF.FormattingEnabled = true;
            this.cboUF.Items.AddRange(new object[] {
            "SP",
            "RJ",
            "CE",
            "BA",
            "MG",
            "AC",
            "AM",
            "TO",
            "PA",
            "PI",
            "PE"});
            this.cboUF.Location = new System.Drawing.Point(165, 192);
            this.cboUF.Name = "cboUF";
            this.cboUF.Size = new System.Drawing.Size(45, 21);
            this.cboUF.TabIndex = 106;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(140, 196);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(22, 17);
            this.label12.TabIndex = 105;
            this.label12.Text = "UF";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(125, 259);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 17);
            this.label11.TabIndex = 104;
            this.label11.Text = "Bairro";
            // 
            // txtnumero
            // 
            this.txtnumero.Location = new System.Drawing.Point(356, 236);
            this.txtnumero.Name = "txtnumero";
            this.txtnumero.Size = new System.Drawing.Size(95, 20);
            this.txtnumero.TabIndex = 103;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 216);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 17);
            this.label5.TabIndex = 96;
            this.label5.Text = "Endereço";
            // 
            // txtCEP
            // 
            this.txtCEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.txtCEP.Location = new System.Drawing.Point(51, 192);
            this.txtCEP.Mask = "00000-000";
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.Size = new System.Drawing.Size(68, 21);
            this.txtCEP.TabIndex = 102;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 195);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 17);
            this.label3.TabIndex = 101;
            this.label3.Text = "CEP";
            // 
            // txtEndereço
            // 
            this.txtEndereço.Location = new System.Drawing.Point(10, 236);
            this.txtEndereço.Name = "txtEndereço";
            this.txtEndereço.Size = new System.Drawing.Size(340, 20);
            this.txtEndereço.TabIndex = 100;
            // 
            // txtBairro
            // 
            this.txtBairro.Location = new System.Drawing.Point(128, 279);
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(323, 20);
            this.txtBairro.TabIndex = 99;
            // 
            // txtCidade
            // 
            this.txtCidade.Location = new System.Drawing.Point(287, 194);
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(164, 20);
            this.txtCidade.TabIndex = 98;
            // 
            // txtcomplemento
            // 
            this.txtcomplemento.Location = new System.Drawing.Point(10, 279);
            this.txtcomplemento.Name = "txtcomplemento";
            this.txtcomplemento.Size = new System.Drawing.Size(109, 20);
            this.txtcomplemento.TabIndex = 97;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 259);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 17);
            this.label8.TabIndex = 95;
            this.label8.Text = "Complemento";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(353, 217);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 17);
            this.label4.TabIndex = 94;
            this.label4.Text = "N°";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 390);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 17);
            this.label17.TabIndex = 123;
            this.label17.Text = "Email";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(9, 410);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(421, 20);
            this.txtEmail.TabIndex = 122;
            this.txtEmail.TextChanged += new System.EventHandler(this.txtEmail_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(227, 347);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 17);
            this.label16.TabIndex = 121;
            this.label16.Text = "Celular";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(7, 347);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 17);
            this.label15.TabIndex = 119;
            this.label15.Text = "Telefone ";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.txtCelular);
            this.panel4.Controls.Add(this.lblalterar);
            this.panel4.Controls.Add(this.txtTelefone);
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.btnalterar);
            this.panel4.Controls.Add(this.lblsalvar);
            this.panel4.Controls.Add(this.btncancelar);
            this.panel4.Controls.Add(this.btnsalvar);
            this.panel4.Location = new System.Drawing.Point(1, 336);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(643, 109);
            this.panel4.TabIndex = 125;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // txtCelular
            // 
            this.txtCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.txtCelular.Location = new System.Drawing.Point(227, 27);
            this.txtCelular.Mask = "(00) 0 0000-0000";
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(200, 21);
            this.txtCelular.TabIndex = 104;
            this.txtCelular.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBox2_MaskInputRejected);
            // 
            // lblalterar
            // 
            this.lblalterar.AutoSize = true;
            this.lblalterar.BackColor = System.Drawing.Color.Transparent;
            this.lblalterar.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblalterar.Location = new System.Drawing.Point(576, 72);
            this.lblalterar.Name = "lblalterar";
            this.lblalterar.Size = new System.Drawing.Size(49, 17);
            this.lblalterar.TabIndex = 221;
            this.lblalterar.Text = "Alterar";
            // 
            // txtTelefone
            // 
            this.txtTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.txtTelefone.Location = new System.Drawing.Point(7, 27);
            this.txtTelefone.Mask = "(00) 0000-0000";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(200, 21);
            this.txtTelefone.TabIndex = 103;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(507, 72);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(63, 17);
            this.label27.TabIndex = 220;
            this.label27.Text = "Cancelar";
            // 
            // btnalterar
            // 
            this.btnalterar.Enabled = false;
            this.btnalterar.Image = global::Buffet.Properties.Resources.alterar;
            this.btnalterar.Location = new System.Drawing.Point(572, 36);
            this.btnalterar.Name = "btnalterar";
            this.btnalterar.Size = new System.Drawing.Size(58, 33);
            this.btnalterar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnalterar.TabIndex = 218;
            this.btnalterar.TabStop = false;
            this.btnalterar.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // lblsalvar
            // 
            this.lblsalvar.AutoSize = true;
            this.lblsalvar.BackColor = System.Drawing.Color.Transparent;
            this.lblsalvar.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsalvar.Location = new System.Drawing.Point(455, 72);
            this.lblsalvar.Name = "lblsalvar";
            this.lblsalvar.Size = new System.Drawing.Size(46, 17);
            this.lblsalvar.TabIndex = 219;
            this.lblsalvar.Text = "Salvar";
            // 
            // btncancelar
            // 
            this.btncancelar.Image = global::Buffet.Properties.Resources._1024px_Deletion_icon_svg;
            this.btncancelar.Location = new System.Drawing.Point(510, 36);
            this.btncancelar.Name = "btncancelar";
            this.btncancelar.Size = new System.Drawing.Size(58, 33);
            this.btncancelar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btncancelar.TabIndex = 216;
            this.btncancelar.TabStop = false;
            this.btncancelar.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // btnsalvar
            // 
            this.btnsalvar.Image = global::Buffet.Properties.Resources._678134_sign_check_512;
            this.btnsalvar.Location = new System.Drawing.Point(448, 36);
            this.btnsalvar.Name = "btnsalvar";
            this.btnsalvar.Size = new System.Drawing.Size(58, 33);
            this.btnsalvar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnsalvar.TabIndex = 217;
            this.btnsalvar.TabStop = false;
            this.btnsalvar.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(5, 130);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 19);
            this.label14.TabIndex = 48;
            this.label14.Text = "Contatos";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.btncep);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Location = new System.Drawing.Point(1, 182);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(643, 162);
            this.panel3.TabIndex = 84;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // btncep
            // 
            this.btncep.BackgroundImage = global::Buffet.Properties.Resources.search;
            this.btncep.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btncep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncep.Location = new System.Drawing.Point(118, 10);
            this.btncep.Name = "btncep";
            this.btncep.Size = new System.Drawing.Size(17, 18);
            this.btncep.TabIndex = 128;
            this.btncep.UseVisualStyleBackColor = true;
            this.btncep.Click += new System.EventHandler(this.btncep_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 160);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 19);
            this.label9.TabIndex = 48;
            this.label9.Text = "Endereço";
            // 
            // txtNFor
            // 
            this.txtNFor.Location = new System.Drawing.Point(6, 58);
            this.txtNFor.Name = "txtNFor";
            this.txtNFor.Size = new System.Drawing.Size(250, 20);
            this.txtNFor.TabIndex = 126;
            // 
            // rdnjuridica
            // 
            this.rdnjuridica.AutoSize = true;
            this.rdnjuridica.Location = new System.Drawing.Point(7, 31);
            this.rdnjuridica.Name = "rdnjuridica";
            this.rdnjuridica.Size = new System.Drawing.Size(63, 17);
            this.rdnjuridica.TabIndex = 127;
            this.rdnjuridica.TabStop = true;
            this.rdnjuridica.Text = "Jurídica";
            this.rdnjuridica.UseVisualStyleBackColor = true;
            this.rdnjuridica.CheckedChanged += new System.EventHandler(this.rdnjuridica_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.rdnfisica);
            this.panel1.Controls.Add(this.rdnjuridica);
            this.panel1.Location = new System.Drawing.Point(6, 91);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(106, 66);
            this.panel1.TabIndex = 130;
            // 
            // rdnfisica
            // 
            this.rdnfisica.AutoSize = true;
            this.rdnfisica.Location = new System.Drawing.Point(7, 8);
            this.rdnfisica.Name = "rdnfisica";
            this.rdnfisica.Size = new System.Drawing.Size(54, 17);
            this.rdnfisica.TabIndex = 128;
            this.rdnfisica.TabStop = true;
            this.rdnfisica.Text = "Fisíca";
            this.rdnfisica.UseVisualStyleBackColor = true;
            this.rdnfisica.CheckedChanged += new System.EventHandler(this.rdnfisica_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 129;
            this.label2.Text = "Tipo Pessoa";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(4, 473);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(36, 26);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 297;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Buffet.Properties.Resources.logo1;
            this.pictureBox2.Location = new System.Drawing.Point(287, 456);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(70, 43);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 296;
            this.pictureBox2.TabStop = false;
            // 
            // CadastroFornecedores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(643, 503);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtNFor);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtObsEndereco);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.cboUF);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtnumero);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCEP);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtEndereço);
            this.Controls.Add(this.txtBairro);
            this.Controls.Add(this.txtCidade);
            this.Controls.Add(this.txtcomplemento);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtobsCadastro);
            this.Controls.Add(this.dtpdatasaida);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cbosituacao);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lblcpf);
            this.Controls.Add(this.txtcpf);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CadastroFornecedores";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CadastroFornecedores";
            this.Load += new System.EventHandler(this.CadastroFornecedores_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnalterar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btncancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnsalvar)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbltitulo;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtobsCadastro;
        private System.Windows.Forms.DateTimePicker dtpdatasaida;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbosituacao;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblcpf;
        private System.Windows.Forms.MaskedTextBox txtcpf;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtObsEndereco;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cboUF;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtnumero;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox txtCEP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEndereço;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.TextBox txtcomplemento;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblalterar;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.PictureBox btnalterar;
        private System.Windows.Forms.Label lblsalvar;
        private System.Windows.Forms.PictureBox btncancelar;
        private System.Windows.Forms.PictureBox btnsalvar;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtNFor;
        private System.Windows.Forms.MaskedTextBox txtCelular;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.Button btncep;
        private System.Windows.Forms.RadioButton rdnjuridica;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rdnfisica;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}