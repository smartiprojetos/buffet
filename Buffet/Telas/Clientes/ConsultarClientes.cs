﻿using Buffet.Db.Clientes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class ConsultarClientes : Form
    {
        public ConsultarClientes()
        {
            InitializeComponent();
        }

        //Criando método para carregar GriedView
        private void CarregarGrid()
        {
            Business_Cliente business = new Business_Cliente();
            DTO_Clientes dto = new DTO_Clientes();
            dto.Nome = txtCliente.Text;
            List<DTO_Clientes> consult = business.Consultar(dto);

            dgvCliente.AutoGenerateColumns = false;
            dgvCliente.DataSource = consult;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void label21_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void txtCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            CarregarGrid();
        }

        private void dgvCliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Business_Cliente business = new Business_Cliente();

            
            if (e.ColumnIndex == 3)
            {
                DTO_Clientes dto = dgvCliente.CurrentRow.DataBoundItem as DTO_Clientes;

                CadastroCliente tela = new CadastroCliente();
                tela.LoadScreenAlterar(dto);
                tela.Show();

                //O único código restante!!!!
                this.Hide();
            }

            //Número de posição estava incorreto!
            if (e.ColumnIndex == 4)
            {
                DTO_Clientes dto = dgvCliente.CurrentRow.DataBoundItem as DTO_Clientes;

                DialogResult re = MessageBox.Show("Deseja realmente remover?",
                                                    "Magic Buffet",
                                                    MessageBoxButtons.OK,
                                                    MessageBoxIcon.Question);

                if (re == DialogResult.Yes)
                {
                    business.Remover(dto);

                    //Assim que a linha for removida, a GRIED VIEW será carregada, dando assim, uma sensação de carregamento
                    //Em tempo real UuU
                    CarregarGrid();
                }

                
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void lblclose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
