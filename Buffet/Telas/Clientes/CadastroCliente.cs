﻿using Buffet.Db.Clientes;
using Buffet.Utilitarios;
using Correios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class CadastroCliente : Form
    {
        public CadastroCliente()
        {
            InitializeComponent();
            LoadScreen();

            //Desabilete as labels também. Caso contrário, irá aparecer uma LABEL com o nome ALTERAR
            /*
                A Lógica é bastante simples. Basicamente, quem estiver programando terá que pegar o valor do controle
                No caso uma Label, e após isso, chamar a função Visible e desabilitar para false.

                OBS: Só tem que ter cuidado na hora de chamar o método de Alterar pois, nesse momento, essa mesma LABEL
                Deverá ficar vísivel. Para tal, apenas pegue o nome do controle e atruibua o método VISIBLE passsando o valor
                Para TRUE
            */
        }

        private void label21_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void EnviarConfirmacao()
        {
            string para = txtEmail.Text;
            string copiaoculta = "smarti7empresa@gmail.com";
            string copia = "smarti7empresa@gmail.com";
            string assunto = "Cadastro Magic Buffet";
            bool comhtml = true;
            //string mensagem =
            //    "Olá," 
            //    +txtnome.Text
            //    + " agradecemos por realizar seu cadastro no sistema de CRM da Magic Buffet"
            //    + " Garantimos que a sua comemoração será inesquecivel!! " +
            //    "/n/n Magic Buffet /n Agradecemos a sua referencia.";

            string mensagem = @"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>


<html xmlns='http://www.w3.org/1999/xhtml'>

 <head>
 	<!--Import Google Icon Font-->
      <link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'>
      <!--Import materialize.css-->
      <link type='text/css' rel='stylesheet' href='css/materialize.min.css'  media='screen,projection'/>

  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
  <title>Magic Buffet</title>
  <meta name='viewport' content='width=device-width, initial-scale=1.0'/>
</head>


<body style='margin: 0; padding: 0;'>


 <table align='center' border='0' cellpadding='0' cellspacing='0' width='600'>
 <tr>
  <td align='center' bgcolor='#70bbd9' style='padding: 40px 0 30px 0;'>
 <img src='https://cdn.icon-icons.com/icons2/122/PNG/512/email_socialnetwork_20049.png' alt='Magic Buffet' width='300' height='230' style='display: block;' />
</td>
 </tr>

 <tr>
  <td bgcolor='#ffffff' style='padding: 40px 30px 40px 30px;'>


 <table border='0' cellpadding='0' cellspacing='0' width='100%'>
 <tr>

  <td>
   <h1>Magic Buffet</h1>
  </td>
 </tr>
 <tr>
  <td style='padding: 20px 0 30px 0;'>
   Olá,
      agradecemos por realizar seu cadastro no sistema de CRM da Magic Buffet.
      Garantimos que a sua comemoração será inesquecivel!!
  </td>
 </tr>

 <tr>
  <td>
   <table border = '0' cellpadding = '0' cellspacing = '0' width = '100%'>
       
        <tr>
       
         <td width = '260' valign = 'top'>
          
             <table border = '0' cellpadding = '0' cellspacing = '0' width = '100%'>
                 
                     <tr>
                 
                      <td>
                 
                       <img src = 'https://static.baratocoletivo.com.br/2017/1017/10013999/g_fast-festas-kids-buffet-infantilcca15a1acd.jpg' alt = '' width = '100%' height = '140' style = 'display: block;' />
                          
                               </td>
                          
                              </tr>
                          
                              <tr>
                          
                               <td style = 'padding: 25px 0 0 0;' >
                                    As melhores festas
     </td >
    </tr >
   </table >
  </td >
  <td style = 'font-size: 0; line-height: 0;' width = '20' >
      &nbsp;
  </td >
  <td width = '260' valign = 'top' >
   
      <table border = '0' cellpadding = '0' cellspacing = '0' width = '100%' >
          
              <tr >
          
               <td >
          
                <img src = 'http://inspireblog.com.br/wp-content/uploads/2015/10/festa-infantil-halloween-festa-com-gosto-inspire-1.jpg' alt = '' width = '100%' height = '140' style = 'display: block;' />
                   
                        </td >
                   
                       </tr >
                   
                       <tr >
                   
                        <td style = 'padding: 25px 0 0 0;' >
                          Serviço de qualidade
     </td >
    </tr >
   </table >
  </td >
 </tr >
</table >
  </td >
 </tr >
</table >


</td >
 </tr >
 <tr >
  <td bgcolor = '#ee4c50' style = 'padding: 30px 30px 30px 30px;' >
   
    <table border = '0' cellpadding = '0' cellspacing = '0' width = '100%' >
          
           <tr >
          
            <td align = 'right' >
           
            <table border = '0' cellpadding = '0' cellspacing = '0' >
                
                  <tr >
                
                   <td >
                
                    <a href = 'http://www.twitter.com/' >
                 
                      <img src = 'https://cdn.icon-icons.com/icons2/838/PNG/512/circle-twitter_icon-icons.com_66835.png' alt = 'Twitter' width = '38' height = '38' style = 'display: block;' border = '0' />
                            
                                </a >
                            
                               </td >
                            
                               <td style = 'font-size: 0; line-height: 0;' width = '20' > &nbsp;</ td >
                                   
                                      <td >
                                   
                                       <a href = 'http://www.twitter.com/' >
                                    
                                         <img src = 'https://png.pngtree.com/element_our/md/20180301/md_5a9794da1b10e.png' alt = 'Facebook' width = '38' height = '38' style = 'display: block;' border = '0' />
                                               
                                                   </a >
                                               
                                                  </td >
                                               
                                                 </tr >
                                               
                                                </table >
                                               </td >
                                               
                                                </tr >
                                               </table >
                                               </td >
                                               
                                                </tr >
                                               </table >
                                               


                                               </body >
                                               


                                               </html >
                                               

                                               <style '>
@import url('https://fonts.googleapis.com/css?family=Dancing+Script');
            h1{

                font - family: 'Dancing Script', cursive;
                text - align: center;
            }


</ style > ";


            Email email = new Email();
            email.EnviarGmail(para, copia, copiaoculta, assunto, mensagem, comhtml);

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {


            DTO_Clientes dto = new DTO_Clientes();

            //Mudando um pouco do método para não dar conflito
            SalvarNha(dto);

            OBUSINESSVEMAQUI.Salvar(dto);


            MessageBox.Show("Cliente cadastrado com sucesso!",
                            "Magic Buffet",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
            EnviarConfirmacao();




        }
        //Essa objeto deve ficar como uma variável não instânciada, 
        //pois caso contrário, ela não poderá servir para todos os métodos
        DTO_Clientes nha;

        Business_Cliente OBUSINESSVEMAQUI = new Business_Cliente();

        public void LoadScreenAlterar(DTO_Clientes nha)
        {
            //O objeto de escopo acima estará recebendo o valor de 
            this.nha = nha;

            
            txtnome.Text = nha.Nome;
            txtcpf.Text = nha.CPF;
            txtprimeiraOB.Text = nha.Observacao_Cadastro;
            dtpnascimento.Value = nha.Data_Nascimento;
           

            //ENDEREÇO
            txtcep.Text = nha.CEP;
            cbouf.SelectedItem = nha.UF;
            txtcidade.Text = nha.Cidade;
            txtendendereco.Text = nha.Endereco;
            txtNumero.Text = nha.Numero;

            //Novamente a escrita do controle estava incorreta!
            txtSegundaOBS.Text = nha.Observacoes_Endereco;
            TxtComplemento.Text= nha.Complemento;
            txtbairro.Text = nha.Bairro;

            //CONTATOS
            txtTelefone.Text = nha.Telefone;
            txtCelular.Text = nha.Celular;
            txtEmail.Text = nha.Email;

            btnalterar.Enabled = true;
            btnalterar.Visible = true;
            lblalterar.Enabled = true;
            lblalterar.Visible = true;

            btnsalvar.Enabled = false;
            btnsalvar.Visible = false;
            lblsalvar.Enabled = false;
            lblsalvar.Visible = false;
        }

        private void SalvarNha(DTO_Clientes dto)
        {
            try
            {
                //Validacao vali = new Validacao();

                //if (vali.VerificarNome(txtnome.Text) == false)
                //{
                //    MessageBox.Show("Verifique se o nome foi digitado corretamente",
                //                       "Magic Buffet",
                //                       MessageBoxButtons.OK,
                //                       MessageBoxIcon.Error);
                //    return;
                //}


                //if (vali.VerificarCPF(txtcpf.Text) == false || txtcpf.Text == null)
                //{
                //    MessageBox.Show("Verifique se o cpf foi digitado corretamente",
                //                      "Magic Buffet",
                //                      MessageBoxButtons.OK,
                //                      MessageBoxIcon.Error);
                //    return;
                //}


                dto.CPF = txtcpf.Text;
                dto.Nome = txtnome.Text;
                dto.Observacao_Cadastro = txtprimeiraOB.Text;
                dto.Data_Nascimento = dtpnascimento.Value;
                dto.Data_Cadastro = DateTime.Now;

                //ENDEREÇO
                dto.CEP = txtcep.Text;
                dto.UF = cbouf.SelectedItem.ToString();
                dto.Cidade = txtcidade.Text;
                dto.Endereco = txtendendereco.Text;
                dto.Numero = txtNumero.Text;

                //Esta parte estava uma cópia da NHA acima.
                dto.Observacoes_Endereco = txtSegundaOBS.Text;
                dto.Complemento = TxtComplemento.Text;
                dto.Bairro = txtbairro.Text;

                //CONTATOS
                dto.Telefone = txtTelefone.Text;
                dto.Celular = txtCelular.Text;
                dto.Email = txtEmail.Text;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show( "\n" + ex.Message,
                                 "Magic Buffet",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
           
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            //Mudando um pouco do método para não dar conflito
            //Pois como o método de ALTERAR precisa das informações, eu apenas chama a variável de escopo
            SalvarNha(nha);
            OBUSINESSVEMAQUI.Alterar(nha);
            

            MessageBox.Show("Cliente alterado com sucesso!",
                            "Magic Buffet",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
            Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Hide();

        }

        private void LoadScreen()
        {
            UF uf = new UF();
            cbouf.DataSource = uf.UFS();



            btnalterar.Enabled = false;
            btnalterar.Visible = false;
            lblalterar.Enabled = false;
            lblalterar.Visible = false;

            btnsalvar.Enabled = true;
            btnsalvar.Visible = true;
            lblsalvar.Enabled = true;
            lblsalvar.Visible = true;
        }

        private void btncep_Click(object sender, EventArgs e)
        {
            ProcurarCEP();
        }
      
        private void ProcurarCEP()
        {
            try
            {
                string cep = txtcep.Text;
                var service = new CorreiosApi();
                var dados = service.consultaCEP(cep);
                txtcidade.Text = dados.cidade;
                txtendendereco.Text = dados.end;
                txtbairro.Text = dados.bairro;
                cbouf.SelectedItem = dados.uf;

            }
            catch 
            {
                MessageBox.Show("CEP não encontrado. Certifique-se de que o número informado está correto. ", "Magic Buffet", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);

            }
        }

        private void txtCIdade_TextChanged(object sender, EventArgs e)
        {

        }

        private void label25_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label21_Click_1(object sender, EventArgs e)
        {

            this.WindowState = FormWindowState.Minimized;
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {
            string email = txtEmail.Text;

            Regex rg = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");

            bool validou = rg.IsMatch(txtEmail.Text);
            if (validou == true)
            {
                txtEmail.BackColor = Color.LawnGreen;
            }
            else
            {
                txtEmail.BackColor = Color.IndianRed;
            }
        }

        private void CadastroCliente_Load(object sender, EventArgs e)
        {

        }
    }
}

