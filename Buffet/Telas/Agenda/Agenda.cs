﻿using Buffet.Db.Agenda;
using Buffet.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas.Agenda
{
    public partial class Agenda : Form
    {
        public Agenda()
        {
            InitializeComponent();
            
        }

      

        private void button1_Click(object sender, EventArgs e)
        {
            LoadScreen();
            REalizarPedido data = new REalizarPedido();
            //data.LoadScreen(dtpfesta.Value, txthora.ToString());
            
           
            DTO_agenda dto = new DTO_agenda();
            dto.hr_horario = txthora.Text;
            dto.obs_observacao = txtobs.Text;
            dto.dt_dataatual = DateTime.Now;
            dto.dt_datareserva = dtpfesta.Value;

            Business_agenda business = new Business_agenda();
            business.Salvar(dto);

            data.Show();
            Close();
        }

        private void label21_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Agenda_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Data()
        {
           
        }

        private void LoadScreen()
        {
            Horario.Valor = dtpfesta.Value;
            Horario.horas = txthora.Text;
        }
    }
}
