﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas.Agenda
{
    public partial class calendario : Form
    {
        public calendario()
        {
            InitializeComponent();
            //Começa a contagem para o início do SPLASH
            Task.Factory.StartNew(() =>
            {
                //Espera cerca de 5 segundos.
                System.Threading.Thread.Sleep(8000);

                Invoke(new Action(() =>
                {
                    Telas.Agenda.Agenda agenda = new Telas.Agenda.Agenda();
                    agenda.Show();

                    this.Hide();

                }));
            });
        }

        private void calendario_Load(object sender, EventArgs e)
        {

        }
    }
}
