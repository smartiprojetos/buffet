﻿using Buffet.Db.Funcionarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            CarregarPermissoes();
        }

        private void CarregarPermissoes()
        {
            if(UserSession.UsuarioLogado.pr_permissao_cliente == false)
            {
                clienteToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_cadastrar_cliente == false)
            {
                cadastroToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_consultar_cliente == false)
            {
                consultarToolStripMenuItem1.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_atualiza_cadastror_cliente == false)
            {
                atualizarCadastroToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_pedido == false)
            {
                pedidoToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_novo_pedido == false)
            {
                realizarNovoPedidoToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_atualizar_pedido == false)
            {
                atualizarPedidoToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_consultar_pedido == false)
            {
                consultarPedidosToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_funcionario == false)
            {
                funcionarioToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_cadastrar_funcionario == false)
            {
                cadastrarNovoFuncionárioToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_novo_funcionario == false)
            {
                novoCadastroToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_alterar_dados_funcionarios == false)
            {
                alterarDadosToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_consultar_funcionarios == false)
            {
                consultarFuncionáriosToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_fornecedor == false)
            {
                fornecedoresToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_cadastrar_fornecedor == false)
            {
                cadastrarToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_consultar_fornecedor == false)
            {
                consultarToolStripMenuItem2.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_estoque == false)
            {
                estoqueToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_consultar_estoque == false)
            {
                consultarEstoqueToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_alterar_estoque == false)
            {
                alterarEstoqueToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_perdir_ao_fornecedor == false)
            {
                pedirAoFornecedorToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_cadastrar_terceiriziados == false)
            {
                cadastrarToolStripMenuItem1.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permissao_consultar_terceriziado == false)
            {
                consultarToolStripMenuItem3.Enabled = false;
            }

            if (UserSession.UsuarioLogado.pr_permisssao_terceirizados == false)
            {
                terceirizadosToolStripMenuItem.Enabled = false;
            }
        }

        private void cadastrarNovoFuncionárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.CadastroFuncionarios yy = new Telas.CadastroFuncionarios();
            yy.Show();

        }

        private void novoCadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.CadastroFuncionarios oi = new Telas.CadastroFuncionarios();
            oi.Show();



        }

        private void label7_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.CadastroCliente oi1 = new Telas.CadastroCliente();
            oi1.Show();

        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.CadastroFornecedores oi2 = new Telas.CadastroFornecedores();
            oi2.Show();

        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Telas.CadastroTerceirizados oi3 = new Telas.CadastroTerceirizados();
            oi3.Show();


        }

        private void consultarFuncionáriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.ConsultarFuncionarios oi4 = new Telas.ConsultarFuncionarios();
            oi4.Show();

        }

        private void consultarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Telas.ConsultarTerceirizados oi5 = new Telas.ConsultarTerceirizados();
            oi5.Show();

        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Telas.ConsultarFornecedores oi6 = new Telas.ConsultarFornecedores();
            oi6.Show();


        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Telas.ConsultarClientes oi7 = new Telas.ConsultarClientes();
            oi7.Show();
        }

        private void realizarNovoPedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.REalizarPedido ei1 = new Telas.REalizarPedido();
            ei1.Show();
        }

        private void atualizarPedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Telas.REalizarPedido ei1 = new Telas.REalizarPedido();
            ei1.Show();

        }

        private void atualizarCadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.CadastroCliente oi1 = new Telas.CadastroCliente();
            oi1.Show();


        }

        private void button3_Click(object sender, EventArgs e)
        {
            Telas.TesteLogin ei2 = new Telas.TesteLogin();
            ei2.Show();

        }

        private void alterarDadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.CadastroFuncionarios e6 = new Telas.CadastroFuncionarios();
            e6.Show();

        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
           


        }

        private void pedirAoFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.PedidoFornecedor ee = new Telas.PedidoFornecedor();
            ee.Show();
            

        }

        private void consultarEstoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.Estoque uu = new Telas.Estoque();
            uu.Show();

        }

        private void estoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void alterarEstoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.EditarEstoque uu = new Telas.EditarEstoque();
            uu.Show();
        }

        private void consultarPedidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.ConsultarReservas ii = new Telas.ConsultarReservas();
            ii.Show();

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void folhaDePagamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.FolhadePagamento oi = new Telas.FolhadePagamento();
            oi.Show();
            Hide();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            DialogResult re = MessageBox.Show("Deseja mesmo fechar o programa?",
                                               "Magic Buffet",
                                               MessageBoxButtons.YesNo,
                                               MessageBoxIcon.Question);
            if (re == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void label10_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

            DialogResult re = MessageBox.Show("Deseja efetuar Logoff?",
                                               "Magic Buffet",
                                               MessageBoxButtons.YesNo,
                                               MessageBoxIcon.Question);
            if (re == DialogResult.Yes)
            {
                Telas.LoginUsuarioComum login = new Telas.LoginUsuarioComum();
                login.Show();
                Close();
            }


            
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void fluxoDeCaixaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.Fluxo_de_Caixa.FluxoCaixa fluxo = new Telas.Fluxo_de_Caixa.FluxoCaixa();
            fluxo.Show();
        }
    }
}
