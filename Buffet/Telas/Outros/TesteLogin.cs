﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class TesteLogin : Form
    {
        public TesteLogin()
        {
            InitializeComponent();
        }

        private void escolhe1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Telas.LoginUsuarioComum e5 = new Telas.LoginUsuarioComum();
            e5.Show();
            Hide();


        }

        private void button2_Click(object sender, EventArgs e)
        {
            Telas.CadastrarUsuario ei3 = new Telas.CadastrarUsuario();
            ei3.Show();
            Hide();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Close();

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("Você tem permissão para cadastrar um novo usuário?", "Magic Buffet", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            Telas.CadastrarUsuario ei3 = new Telas.CadastrarUsuario();
            ei3.Show();
            Hide();
        }

        private void label7_Click_1(object sender, EventArgs e)
        {
            Form1 oi = new Form1();
            oi.Show();
            Hide();
        }
    }
}
