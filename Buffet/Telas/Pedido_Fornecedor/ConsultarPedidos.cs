﻿using Buffet.Db.Fornecedores.Pedido_Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas.Pedido_Fornecedor
{
    public partial class ConsultarPedidos : Form
    {
        public ConsultarPedidos()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            Business_PedidoFornecedor business = new Business_PedidoFornecedor();
            
            if (e.ColumnIndex == 5)
            {
                //pega os valores da linha selecionada e envia para o dto 
                DTO_Pedidofornecedor dto = dtgConsultar.CurrentRow.DataBoundItem as DTO_Pedidofornecedor;
                DialogResult re = MessageBox.Show("Deseja realmente remover?",
                                                   "Magic Buffet",
                                                   MessageBoxButtons.OK,
                                                   MessageBoxIcon.Question);
                if (re == DialogResult.Yes)
                {
                business.Remover(dto.ID);
                    CarregarDados();
                }
                
            }
            if (e.ColumnIndex == 6)
            {
                DTO_Pedidofornecedor dto = dtgConsultar.CurrentRow.DataBoundItem as DTO_Pedidofornecedor;
                PedidoFornecedor tela = new PedidoFornecedor();
                tela.LoadScreenAlterar();


            }
        }

        private void CarregarDados()
        {
            Business_PedidoFornecedor business = new Business_PedidoFornecedor();

            DTO_Pedidofornecedor dto = new DTO_Pedidofornecedor();
            // string nome = dto.Nome_Produto = Convert.ToString(Nome_Produto);
            string nome = txtproduto.Text;

            dtgConsultar.AutoGenerateColumns = false;
            dtgConsultar.DataSource = business.Consultar(nome);

          
         
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Telas.PedidoFornecedor tela = new Telas.PedidoFornecedor();
            tela.Show();
            Hide();
        }

        private void label21_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }

        private void ConsultarPedidos_Load(object sender, EventArgs e)
        {

        }
    }
}

