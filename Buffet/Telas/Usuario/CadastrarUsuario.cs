﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class CadastrarUsuario : Form
    {
        public CadastrarUsuario()
        {
            InitializeComponent();
        }

        private void label21_Click(object sender, EventArgs e)
        {
            Hide();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Usuário cadastrado com sucesso!", "Magic Buffet", MessageBoxButtons.OK , MessageBoxIcon.Information);
            this.Hide();



        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();

        }
    }
}
