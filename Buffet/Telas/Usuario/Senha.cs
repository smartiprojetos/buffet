﻿using Buffet.Db.Funcionarios;
using Buffet.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas.Usuario
{
    public partial class Senha : Form
    {
        public Senha()
        {
            InitializeComponent();
        }
        CriacaodeCodigo cod;
        string codigo;

        private void EnviarConfirmacao()
        {

            cod = new CriacaodeCodigo();
             codigo = cod.Codigo4();

            string para = txtemail.Text;
            string copiaoculta = "smarti7empresa@gmail.com";
            string copia = "smarti7empresa@gmail.com";
            string assunto = "Modificação de senha";
            bool comhtml = true;
            string mensagem =
                "Olá, informamos que o usuario " +
                "  "
                + txtnomeusuario.Text
                + " está realizando o pedido para efetuar a " +
                "  "

                + " troca de senha, caso não seja você, ignore esta mensagem " +
                "  "
                + " caso seja envie o codigo abaixo " +
                "  "
                + codigo;

            Email email = new Email();
            email.EnviarGmail(para, copia, copiaoculta, assunto, mensagem, comhtml);
            MessageBox.Show("Email com o código enviado",
                                "Magic Buffet",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
        }

        private bool ConferirCOdigo()
        {
            bool codcorreto;
            string codinformado = txtcod.Text;
            if (codigo == codinformado)
            {
                MessageBox.Show("Troca de senha realizada com sucesso",
                                "Magic Buffet",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                codcorreto = true;
                
            }
            else
            {
               DialogResult re = MessageBox.Show("Codigo incorreto, deseja enviar um novo código ?",
                                  "Magic Buffet",
                                  MessageBoxButtons.YesNoCancel,
                                  MessageBoxIcon.Information);
                if (re == DialogResult.Yes)
                {
                    EnviarConfirmacao();
                }
                if (re == DialogResult.Cancel)
                {
                    LoginUsuarioComum tela = new LoginUsuarioComum();
                    tela.Show();
                    this.Close();
                }
               
                codcorreto = false;
            }
            return codcorreto;
        }

        private void btnenviaremail_Click(object sender, EventArgs e)
        {
            EnviarConfirmacao();
        }

        private void btncod_Click(object sender, EventArgs e)
        {
           bool cod = ConferirCOdigo();

            if (cod == true)
            {
                Business_Funcionario bus = new Business_Funcionario();

                List<DTO_Fucionario> lista= bus.ListarporUsuario(txtnomeusuario.Text);
                DTO_Fucionario dto = lista[0];
                bus.AlterarSenha(dto.ID_Funcionario, txtnovasenha.Text);

                MessageBox.Show("Senha alterada com sucesso",
                                "Magic Buffet",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);

            }
        }

        private void label21_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Senha_Load(object sender, EventArgs e)
        {

        }

        private void txtcod_TextChanged(object sender, EventArgs e)
        {

        }
        private void nha()
        {

            string senha = txtnovasenha.Text;
            string ex = @"^.*(?=.{5,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$";
            Regex re = new Regex(ex);
            bool validar = re.IsMatch(txtnovasenha.Text);
            if (validar == true)
            {
                txtnovasenha.BackColor = Color.LawnGreen;
            }
            else
            {
                txtnovasenha.BackColor = Color.IndianRed;
            }
        }
        private void txtemail_TextChanged(object sender, EventArgs e)
        {
            string email = txtemail.Text;

            Regex rg = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");

            bool validou = rg.IsMatch(txtemail.Text);
            if (validou == true)
            {
                txtemail.BackColor = Color.LawnGreen;
            }
            else
            {
                txtemail.BackColor = Color.IndianRed;
            }
        }

        private void txtsenhanovamente_TextChanged(object sender, EventArgs e)
        {
            if (txtsenhanovamente.Text == txtnovasenha.Text)
            {
                txtsenhanovamente.BackColor = Color.LawnGreen;
            }
            else
            {
                txtsenhanovamente.BackColor = Color.IndianRed;
            }
        }

        private void txtnovasenha_TextChanged(object sender, EventArgs e)
        {
            nha();
        }
    }
}
