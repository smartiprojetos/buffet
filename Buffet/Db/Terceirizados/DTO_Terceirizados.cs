﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Terceirizados
{
    public class DTO_Terceirizados
    {
        public int ID { get; set; }
        public string nome_Fantasia { get; set; }
        public string CPF_CNPJ { get; set; }
        public string Situacao { get; set; }
        public DateTime Data_Cadastro { get; set; }
        public DateTime Data_Saida { get; set; }
        public string telefone { get; set; }
        public string celular { get; set; }
        public string email { get; set; }
        public string observacoes { get; set; }
    }
}
