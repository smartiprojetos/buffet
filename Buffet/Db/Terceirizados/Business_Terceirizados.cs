﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Terceirizados
{
    class Business_Terceirizados
    {

        public int Salvar (DTO_Terceirizados dto)
        {

            if (dto.nome_Fantasia == null)
            {
                throw new ArgumentException("O CPF é obrigatório!");
            }
            if (dto.CPF_CNPJ == null)
            {
                throw new ArgumentException("O Nome é obrigatório!");
            }
            if (dto.Situacao == null)
            {
                throw new ArgumentException("A data de nascimento é obrigatória!");
            }

            if (dto.telefone == null && dto.celular == null)
            {
                throw new ArgumentException("O CPF é obrigatório!");
            }
            Database_Terceirizados data = new Database_Terceirizados();

            return data.Salvar(dto);
            
        }

        public List<DTO_Terceirizados> Listar()
        {
            Database_Terceirizados data = new Database_Terceirizados();
            return data.Listar();
          
        }

        //Sobrecarga
        public List<DTO_Terceirizados> Consultar(DTO_Terceirizados dto)
        {
            Database_Terceirizados data = new Database_Terceirizados();
            return data.Consultar(dto);

        }

        public void Alterar(DTO_Terceirizados dto)
        {
            Database_Terceirizados data = new Database_Terceirizados();
            data.Alterar(dto);
        }

        public void Remover(DTO_Terceirizados dto)
        {
            Database_Terceirizados data = new Database_Terceirizados();
            data.Remover(dto);
        }



    }
}
