﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Estoque
{
    class Business_Estoque
    {


        public int Salvar(DTO_Estoque dto)
        {
            if (dto.Data_Atual == null && dto.Quantidae_Atual == 0)
            {
                throw new ArgumentException("O CPF é obrigatório!");
            }
            if (dto.Data_Entrada == null && dto.Quantidade_Entrada == 0)
            {
                throw new ArgumentException("O Nome é obrigatório!");
            }
            if (dto.Data_Saida == null && dto.Quantidade_Saida == 0)
            {
                throw new ArgumentException("A data de nascimento é obrigatória!");
            }

            if (dto.Produto == null)
            {
                throw new ArgumentException("O CPF é obrigatório!");
            }

            if (dto.Data_Validade == null )
            {
                throw new ArgumentException("O CPF é obrigatório!");
            }


            Database_Estoque Db = new Database_Estoque();
            return Db.Salvar(dto);
        }

        public void Alterar(DTO_Estoque dto)
        {
            Database_Estoque Db = new Database_Estoque();
            Db.Alterar(dto);
        }

        public List<DTO_Estoque> Listar()
        {
            Database_Estoque Db = new Database_Estoque();
            List<DTO_Estoque> list = Db.Lista();
            return Db.Lista();
        }
        public List<DTO_Estoque> Listar(string produto)
        {
            Database_Estoque Db = new Database_Estoque();
            List<DTO_Estoque> list = Db.Lista(produto);
            return Db.Lista();
        }



        public void Remover(DTO_Estoque dto)
        {
            Database_Estoque Db = new Database_Estoque();
            Db.Remover(dto);
        }



    }
} 
