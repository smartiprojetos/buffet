﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Estoque
{
    public class DTO_Estoque
    {
        public int id { get; set; }
        public string Produto { get; set; }
        public int Quantidade_Entrada { get; set; }
        public DateTime Data_Entrada { get; set; }
        public int Quantidade_Saida { get; set; }
        public DateTime Data_Saida { get; set; }
        public int Quantidae_Atual { get; set; }
        public DateTime Data_Atual { get; set; }
        public DateTime Data_Validade { get; set; }
        
    }
}
