﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Pedidos
{
    class Database_Pedidos
    {
        public int Salvar(DTO_Pedidos dto)
        {
            string script = @"Insert into   pedido_festa ( 
                                                                 fk_cliente,
                                                                
                                                                 Descricao, 
                                                                 Local_Festa, 
                                                                 Valor_Total,        
                                                                 Valor,              
                                                                 desconto,
                                                                 forma_pagamento,
                                                                 hr_hora,
                                                                 dt_data, 
                                                                 obs_observacao, 
                                                                 dt_dia_do_agendamento 
    
                                                                ) Values (
                                                                    @fk_cliente,
                                                                   
                                                                                @Descricao, 
                                                                    @Local_Festa,
                                                                    @Valor_Total,
                                                                    @Valor,
                                                                    @desconto,
                                                                    @forma_Pagamento,
                                                                    @hr_hora,
                                                                    @dt_data, 
                                                                    @obs_observacao, 
                                                                    @dt_dia_do_agendamento
                                                                )  ";


            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("fk_cliente", dto.fk_cliente));
            
            parm.Add(new MySqlParameter("Descricao", dto.descricao));
            parm.Add(new MySqlParameter("Local_Festa", dto.local));
            parm.Add(new MySqlParameter("Valor_Total", dto.Valor_Total));
            parm.Add(new MySqlParameter("Valor", dto.Valor));
            parm.Add(new MySqlParameter("desconto", dto.desconto));
            parm.Add(new MySqlParameter("forma_Pagamento", dto.forma_Pagamento));
            parm.Add(new MySqlParameter("hr_hora", dto.hr_horario));
            parm.Add(new MySqlParameter("dt_data", dto.dt_dataatual));
            parm.Add(new MySqlParameter("obs_observacao", dto.obs_observacao));
            parm.Add(new MySqlParameter("dt_dia_do_agendamento", dto.dt_datareserva));

            Database db = new Database();

            return db.ExecuteInsertScriptWithPk(script, parm);


        }
        public void Remover(int id)
        {
            string script = @"DELETE FROM pedido_festa WHERE ID_PedidoFesta = @ID_PedidoFesta";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PedidoFesta", id));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(DTO_Pedidos dto)
        {
            string script = @" update pedido_festa Set 
                                                        fk_cliente =            @fk_cliente,
                                                       
                                                        Descricao =             @Descricao,
                                                        Local_Festa =                 @Local_Festa,
                                                        Valor_Total =           @Valor_Total, 
                                                        Valor =                 @Valor,
                                                        desconto =              @desconto,
                                                         forma_Pagamento =      @pagamento,
                                                        hr_hora =               @hr_hora,
                                                        dt_dia_do_agendamento = @dt_dia_do_agendamento,
                                                        obs_observacao =        @obs_observacao";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("fk_cliente", dto.fk_cliente));
           
            parm.Add(new MySqlParameter("Descricao", dto.descricao));

            parm.Add(new MySqlParameter("Local_Festa", dto.local));
            parm.Add(new MySqlParameter("Valor_Total", dto.Valor_Total));
            parm.Add(new MySqlParameter("Valor", dto.Valor));
            parm.Add(new MySqlParameter("desconto", dto.desconto));
            parm.Add(new MySqlParameter("forma_Pagamento", dto.forma_Pagamento));

            Database db = new Database();
            db.ExecuteSelectScript(script, parm);



        }


        public List<DTO_Pedidos> List()
        {
            string script = @"Select *  from Pedido_Festa";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Pedidos> lista = new List<DTO_Pedidos>();
            while (reader.Read())
            {

                DTO_Pedidos dto = new DTO_Pedidos();
                dto.id = reader.GetInt32("id");
                dto.fk_cliente = reader.GetInt32("fk_cliente");
               
                dto.desconto = reader.GetDecimal("desconto");
                dto.descricao = reader.GetString("Descricao");
                dto.forma_Pagamento = reader.GetString("forma_Pagamento");
                dto.Valor = reader.GetDecimal("Valor");
                dto.Valor_Total = reader.GetDecimal("Valor_Total");
                dto.local = reader.GetString("Local_Festa");

                lista.Add(dto);



            }
            reader.Close();
            return lista;


        }

        public List<DTO_Consultar_Pedido> ConsultarPedidos(string nome)
        {
            string script = @"select * from consultar_pedidos where Nome Like @Nome";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", nome + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Consultar_Pedido> lista = new List<DTO_Consultar_Pedido>();
            while (reader.Read())
            {
                DTO_Consultar_Pedido dto = new DTO_Consultar_Pedido();
                dto.NomeCliente = reader.GetString("Nome");
                dto.DataAgendamento = reader.GetDateTime("dt_dia_do_agendamento");
                dto.DataFesta = reader.GetDateTime("dt_data");
                dto.LocaldaFesta = reader.GetString("Local_Festa");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<DTO_mes> Listarmes()
        {
            string script = @"Select *  from tb_mes";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_mes> lista = new List<DTO_mes>();
            while (reader.Read())
            {

                DTO_mes dto = new DTO_mes();
                dto.id_mes = reader.GetInt32("id_mes");
                dto.mes = reader.GetString("ds_mes");

                lista.Add(dto);



            }
            reader.Close();
            return lista;

        }






    }
}