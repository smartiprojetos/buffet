﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Pedidos
{
    class Business_Pedidos
    {


        Database_Pedidos database_Pedidos = new Database_Pedidos();
        public int Salvar(DTO_Pedidos dto)
        {

            if (dto.local == null)
            {
                throw new ArgumentException("O CPF é obrigatório!");
            }
            if (dto.Valor_Total == null && dto.Valor == null && dto.desconto == null)
            {
                throw new ArgumentException("O Nome é obrigatório!");
            }
            if (dto.forma_Pagamento == null)
            {
                throw new ArgumentException("A data de nascimento é obrigatória!");
            }

            


            Database_Pedidos db = new Database_Pedidos();
            int pk = db.Salvar(dto);
            return pk;
        }

        public void Alterar(DTO_Pedidos dto)
        {
            database_Pedidos.Alterar(dto);
        }

        public void Remover(int id)
        {
            database_Pedidos.Remover(id);
        }

        public List<DTO_Consultar_Pedido> consultarPedido (string nome)
        {
            return database_Pedidos.ConsultarPedidos(nome); 
            
        }
        public List<DTO_mes> Mes()
        {
            DTO_mes dto = new DTO_mes();
            Database_Pedidos db = new Database_Pedidos();
         return   db.Listarmes();
        }

    }
}
