﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Pedidos
{
    class DTO_Consultar_Pedido
    {
        public string NomeCliente { get; set; }
        public DateTime DataFesta { get; set; }
        public DateTime DataAgendamento { get; set; }
        public string LocaldaFesta { get; set; }
    }
}
