﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Fornecedores
{
    public class DTO_Fornecedores
    {

        public int ID_Fornecedor { get; set; }
        public string Nome_Fantasia  { get; set; }
        public string CPF_CNPJ { get; set; }
        public string Situacao { get; set; }
        public DateTime Data_Cadastro { get; set; }
        public DateTime Data_Saida { get; set; }
        public string observacao_Cadastro { get; set; }
        public string CEP { get; set; }
        public string UF { get; set; }
        public string Cidade { get; set; }
        public string Endereco { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Numero { get; set; }
        public string Observacoes_Endereco { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
    
        

    }
}
