﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Agenda
{
    class Business_agenda
    {
        public List<DTO_agenda> Listar()
        {
            Database_agenda db = new Database_agenda();
            return db.Listar();
        }

        public int Salvar(DTO_agenda dto)
        {
           
            if (dto.hr_horario == null)
            {
                throw new ArgumentException("O horario é obrigatorio!");
            }
            if (dto.obs_observacao == null  )
            {
                throw new ArgumentException("observe algo!");
            }
            Database_agenda database_Agenda = new Database_agenda();
            int pk = database_Agenda.Salvar(dto);
            return pk;
        }
    }
}
