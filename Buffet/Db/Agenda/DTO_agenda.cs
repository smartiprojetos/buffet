﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Agenda
{
   public class DTO_agenda
    {


        public int ID_agenda { get; set; }
        public string hr_horario { get; set; }
        public string obs_observacao { get; set; }
        public DateTime dt_datareserva { get; set; }
        public DateTime dt_dataatual { get; set; }


    }
}
