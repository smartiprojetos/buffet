﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading.Tasks;

namespace Buffet.Db.Agenda
{
    class Database_agenda
    {

        public int Salvar(DTO_agenda dto)
        {
            string script = @"INSERT INTO agenda ( 
                                                hr_hora,
                                                dt_data, 
                                                obs_observacao, 
                                                dt_dia_do_agendamento 
                                                ) 
                                                VALUES
                                                (
                                                @hr_hora,
                                                @dt_data, 
                                                @obs_observacao, 
                                                @dt_dia_do_agendamento
                                                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("hr_hora", dto.hr_horario));
            parms.Add(new MySqlParameter("dt_data", dto.dt_dataatual));
            parms.Add(new MySqlParameter("obs_observacao", dto.obs_observacao));
            parms.Add(new MySqlParameter("dt_dia_do_agendamento", dto.dt_datareserva));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }


        public List<DTO_agenda> Listar()
        {
            string script = @"Select * from agenda";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_agenda> agendas = new List<DTO_agenda>();

            while (reader.Read())
            {
                DTO_agenda dto = new DTO_agenda();
                dto.ID_agenda = reader.GetInt32("idAgenda");
                dto.hr_horario = reader.GetString("hr_hora");
                dto.dt_dataatual = reader.GetDateTime("dt_data");
                dto.obs_observacao= reader.GetString("obs_observacao");
                dto.dt_datareserva = reader.GetDateTime("dt_dia_do_agendamento");

                agendas.Add(dto);
            }

            reader.Close();
            return agendas;
        }

        public void Alterar(DTO_agenda dto)
        {
            string script = @"UPDATE agenda SET 
                                          hr_hora = @hr_hora,
                                        
                                          dt_dia_do_agendamento = @dt_dia_do_agendamento,
                                          obs_observacao = @obs_observacao
                                          
                               WHERE idAgenda = @idAgenda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
     
            parms.Add(new MySqlParameter("hr_hora", dto.hr_horario));
         
            parms.Add(new MySqlParameter("dt_dia_do_agendamento", dto.dt_datareserva));
            parms.Add(new MySqlParameter("obs_observacao", dto.obs_observacao));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

    }
}
