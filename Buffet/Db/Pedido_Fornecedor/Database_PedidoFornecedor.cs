﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Fornecedores.Pedido_Fornecedor
{
    class Database_PedidoFornecedor
    {
        public int Salvar(DTO_Pedidofornecedor dto)

        {
            string script =
                @" INSERT INTO pedido_fornecedor
               ( Nome_Produto,
                Quantidade,
                Data_Pedido,
                Valor_Uni,
                Valor_Total,
                Forma_Pagamento,
                ID_Fornecedor)

               VALUES

                ( @Nome_Produto,
                @Quantidade,
                @Data_Pedido,
                @Valor_Uni,
                @Valor_Total,
                @Forma_Pagamento,
                @ID_Fornecedor) ";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome_Produto", dto.Nome_Produto));
            parms.Add(new MySqlParameter("Quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("Data_Pedido", dto.Data_Pedido));
            parms.Add(new MySqlParameter("Valor_Uni", dto.Valor_Unidade));
            parms.Add(new MySqlParameter("Valor_Total", dto.Valor_Total));
            parms.Add(new MySqlParameter("Forma_Pagamento", dto.forma_Pagamento));
            parms.Add(new MySqlParameter("ID_Fornecedor", dto.fk_fornecedor));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM pedido_fornecedor WHERE ID_PedidoFornecedor = @ID_PedidoFornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PedidoFornecedor", id));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(DTO_Pedidofornecedor dto)
        {
            string script = @"Update pedido_fornecedor SET
                                             Nome_Produto, @Nome_Produto,
                                                                      Quantidade = @Quantidade,
                                                                      Data_Pedido = @Data_Pedido,
                                                                      Valor_Uni = @Valor_Uni,
                                                                      Valor_Total = @Valor_Total,
                                                                      Forma_Pagamento = @Forma_Pagamento, 
                                                                    
                                                                      ID_Fornecedor = @ID_Fornecedor,
                                                    Where ID_PedidoFornecedor = @ID_PedidoFornecedor
                                                                     ) ";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome_Produto", dto.Nome_Produto));
            parms.Add(new MySqlParameter("Quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("Data_Pedido", dto.Data_Pedido));
            parms.Add(new MySqlParameter("Valor_Uni", dto.Valor_Unidade));
            parms.Add(new MySqlParameter("Valor_Total", dto.Valor_Total));
            parms.Add(new MySqlParameter("Forma_Pagamento", dto.forma_Pagamento));
            parms.Add(new MySqlParameter("ID_Fornecedor", dto.fk_fornecedor));




            Database db = new Database();
            db.ExecuteSelectScript(script, parms);



        }

        public List<DTO_Pedidofornecedor> Listar()
        {
            string script = @"Select * from pedido_fornecedores";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            List<DTO_Pedidofornecedor> lista = new List<DTO_Pedidofornecedor>();
            while (reader.Read())
            {
                DTO_Pedidofornecedor dto = new DTO_Pedidofornecedor();
                dto.ID = reader.GetInt32("ID");
                dto.Nome_Produto = reader.GetString(" Nome_Produto");
                dto.Quantidade = reader.GetInt32("Quantidade");
                dto.Data_Pedido = reader.GetDateTime("Data_Pedido");
                dto.Valor_Unidade = reader.GetDecimal("Valor_Uni");
                dto.Valor_Total = reader.GetDecimal("Valor_Total");
                dto.forma_Pagamento = reader.GetString("Forma_Pagamento");
                dto.fk_fornecedor = reader.GetInt32("ID_Fornecedor");

                lista.Add(dto);

            }
            reader.Close();
            return lista;

        }

        public List<DTO_Pedidofornecedor> Consultar(string Produto)
        {
            string script = @"select * from pedido_fornecedor where Nome_Produto like @Nome_Produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome_Produto", Produto + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Pedidofornecedor> lista = new List<DTO_Pedidofornecedor>();
            DTO_Pedidofornecedor dto = null;
            while (reader.Read())
            {
                dto = new DTO_Pedidofornecedor();
               
                dto.Nome_Produto = reader.GetString("Nome_Produto");
                dto.Quantidade = reader.GetInt32("Quantidade");
                dto.Data_Pedido = reader.GetDateTime("Data_Pedido");
                dto.Valor_Unidade = reader.GetDecimal("Valor_Uni");
                dto.Valor_Total = reader.GetDecimal("Valor_Total");
                dto.forma_Pagamento = reader.GetString("Forma_Pagamento");
                dto.fk_fornecedor = reader.GetInt32("ID_Fornecedor");

               
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }


        public List<DTO_Pedidofornecedor> Buscar(int id)
        {
            string script = @"select * from pedido_fornecedor where Nome_Produto like @Nome_Produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID", id + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Pedidofornecedor> lista = new List<DTO_Pedidofornecedor>();
            DTO_Pedidofornecedor dto = null;
            while (reader.Read())
            {
                dto = new DTO_Pedidofornecedor();
               
                dto.Nome_Produto = reader.GetString(" Nome_Produto");
                dto.Quantidade = reader.GetInt32("Quantidade");
                dto.Data_Pedido = reader.GetDateTime("Data_Pedido");
                dto.Valor_Unidade = reader.GetDecimal("Valor_Uni");
                dto.Valor_Total = reader.GetDecimal("Valor_Total");
                dto.forma_Pagamento = reader.GetString("Forma_Pagamento");
                dto.fk_fornecedor = reader.GetInt32("ID_Fornecedor");



                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
    }

}

