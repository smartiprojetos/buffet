﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.FolhaDePagamento
{
    class DTO_FolhaDePagamento
    {
        public int id_folha_de_pagamento { get; set; }
        public double sl_salario_mes { get; set; }
        public double vl_vale_trasporte { get; set; }
        public double vl_vale_refeicao { get; set; }
        public double ad_adiantamento { get; set; }
        public double inss { get; set; }
        public DateTime hr_hora_extra { get; set; }
        public double ds_descontos { get; set; }
        public double sl_salario_base { get; set; }
        public double cl_calculo_base_inss { get; set; }
        public double cl_cauculo_base_fgts { get; set; }
        public double ft_fgts_do_mes { get; set; }
        public double bs_base_calc_irrf { get; set; }
        public double fi_faixa_irrf { get; set; }
        public double tl_total_vencimentos { get; set; }
        public double rf_referencia { get; set; }
        public string ms_mensagem { get; set; }
        public double sl_salario_bruto { get; set; }
        public double pl_plano_de_saude { get; set; }
        public DateTime hr_horas_trabalhadas { get; set; }
        public double sl_hora { get; set; }
        public int dias_trabalhados { get; set; }
        public DateTime dt_datapagamento { get; set; }
        public int fk_id_funcionario_fp { get; set; }
        public int fk_id_empresa_fp { get; set; }



    }

  
  
}
