﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.FluxodeCaixa
{
    class FluxodeCaixaDataBase
    {
        public List<FluxodeCaixaView> FiltrarData(DateTime start, DateTime end)
        {
            string script = "select * from vw_fluxo_caixa where dt_operacao >= @start and dt_operacao <= @end order by dt_operacao, tp_operacao";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("start", start));
            parms.Add(new MySqlParameter("end", end));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FluxodeCaixaView> fluxodeCaixas = new List<FluxodeCaixaView>();
            while (reader.Read() == true)
            {
                FluxodeCaixaView dto = new FluxodeCaixaView();
                dto.dt_operacao = reader.GetDateTime("dt_operacao");
                dto.vl_total = reader.GetDecimal("vl_total");
                dto.to_operacao = reader.GetString("tp_operacao");

                fluxodeCaixas.Add(dto);
            }
            reader.Close();
            return fluxodeCaixas;

        }
    }
}
