﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.FluxodeCaixa
{
    class FluxodeCaixaBusiness
    {
        public List<FluxodeCaixaView> FiltrarData(DateTime start, DateTime end)
        {
            FluxodeCaixaDataBase db = new FluxodeCaixaDataBase();
            return db.FiltrarData(start, end);
        }
    }
}
