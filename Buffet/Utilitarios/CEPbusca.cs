﻿using Correios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Utilitarios
{
    public class CEPbusca
    {
        public void BuscarCep(string cepbusca)
        {
            var service = new CorreiosApi();
            var dados = service.consultaCEP(cepbusca);

            Dto_cep cep = new Dto_cep();
            cep.Bairro = dados.bairro;
            cep.CEP = dados.cep;
            cep.Cidade = dados.cidade;
            cep.Complemento = dados.complemento;
            cep.Complemento2 = dados.complemento2;
            cep.End = dados.end;
            cep.id = dados.id.ToString();
            cep.uf = dados.uf;
         

           
        }
    }
}
