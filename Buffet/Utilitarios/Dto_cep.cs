﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Utilitarios
{
    public class Dto_cep
    {
        public string Bairro { get; set; }
        public string CEP { get; set; }
        public string Cidade { get; set; }
        public string Complemento { get; set; }
        public string Complemento2 { get; set; }
        public string End { get; set; }
        public string id { get; set; }
        public string uf { get; set; }


    }


}
