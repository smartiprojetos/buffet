﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;

namespace Buffet.Utilitarios
{
    class Email
    {

        public void EnviarGmail(string para,
                                string copia,
                                string copiaoculta,
                                string assunto,
                                string mensagem,
                                bool comhtml)
        {
            try
            {
                //Strings com as informações do email que irá enviar o email.
                string origem = "smarti7empresa@gmail.com";
                string senha = "fr4ng0fr1t0";

                //Para quem o email o email será enviado
                MailMessage email = new MailMessage();
                MailAddress emailpara = new MailAddress(para);
                MailAddress emailcopia = new MailAddress(copia);
                MailAddress emailcopiaoculta = new MailAddress(copiaoculta);

                //Indica por quem o email será enviado
                email.From = new MailAddress(origem);

                //Parametros de envio da mensagem
                email.To.Add(emailpara);
                email.CC.Add(emailcopia);
                email.Bcc.Add(emailcopiaoculta);
                email.Subject = assunto;
                email.Body = mensagem;
                email.IsBodyHtml = comhtml;

                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false; //pois ultiliza as do gmail.
                smtp.Credentials = new NetworkCredential(origem, senha);

                smtp.Send(email);

            }

            /*Caso esteja utilizando uma rede que precisa de proxy adicionar :
             *  string proxyIp = "";
             *  int proxyPort = 8080;
             *  WebRequest.DefaultWebProxy = new WebProxy (proxyIp, proxyPort);
             *  SmtpClient smtp = new SmtpClient();
             */


            catch (Exception ex)
            {
                throw new ArgumentException("E-mail invalido");
            }

        }
    }
}
